--index-url https://${GITLAB_TOKEN_USER}:${GITLAB_TOKEN}@gitlab.com/api/v4/projects/30709403/packages/pypi/simple
--extra-index-url https://pypi.python.org/simple

ingredient-phrase-tagger
celery~=5.1.2
beautifulsoup4~=4.10.0
requests~=2.26.0
redis~=3.5.3
eventlet~=0.33.0
