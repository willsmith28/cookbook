FROM python:3.10-slim-buster as build-image

WORKDIR /app

ARG REQUIREMENTS
ARG GITLAB_TOKEN_USER
ARG GITLAB_TOKEN

# create and set virtual env
RUN python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

RUN pip install --upgrade pip
COPY ./requirements /app/requirements
RUN pip install -r requirements/$REQUIREMENTS.txt

FROM registry.gitlab.com/willsmith28/crfpp:crfpp-python AS final-image

WORKDIR /app

# copy and set virtual env
COPY --from=build-image /opt/venv /opt/venv
ENV PATH "/opt/venv/bin:$PATH"

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

RUN useradd cookbookworker
RUN apt-get update && apt-get install --no-install-recommends -yqq \
    libpq-dev \
    netcat

COPY --chown=cookbookworker . /app

USER cookbookworker
ENTRYPOINT [ "/app/entrypoint.sh" ]
