#!/bin/bash
# Wait for a service:port to be open
#
# Example:
#   wait_for_port "postgres" "5432"
wait_for_port() {
    local host="$1" port="$2"
    local index=0
    local TRY_LOOP=10

    while ! nc -z "$host" "$port" >/dev/null 2>&1 < /dev/null; do
        index=$((index + 1))

        if [ "$index" -ge "$TRY_LOOP" ]; then
            echo >&2 "$host:$port still not reachable, giving up"
            exit 1
        fi

        echo "waiting for $host... $index/$TRY_LOOP"
        sleep 5

    done
}

case "$1" in
    worker)
        wait_for_port "redis" "6379"
        shift;

        exec celery --app app.main:app worker --loglevel "${LOGLEVEL:-INFO}" "$@"
        ;;

    *)
        exec "$@"
        ;;
esac
