import re
import tempfile

import requests
from bs4 import BeautifulSoup
from eventlet.green import subprocess
from ingredient_phrase_tagger.training import utils

from app.config import API_PASSWORD, API_URL, API_USER, MODEL_PATH


def tag_ingredient_phrase(ingredients: list[str]) -> list[dict[str, str]]:
    """takes in a list of raw ingredient descritions and returns a list of
    the tagged results

    Args:
        ingredients (list[str]): raw ingredient descritions

    Returns:
        list[dict[str, str]]: tagged ingredient described below
        {
            "qty": "2",
            "unit": "tablespoon",
            "name": "honey",
            "input": "2 tablespoons honey"
        }
    """
    with tempfile.NamedTemporaryFile(mode="w") as input_file:
        input_file.write(
            utils.export_data(
                item if item.endswith("\n") else f"{item}\n" for item in ingredients
            )
        )
        input_file.flush()
        crf_data = subprocess.check_output(
            [
                "crf_test",
                "--verbose=1",
                "--model",
                MODEL_PATH,
                input_file.name,
            ],
        ).decode("utf-8")
        return utils.import_data(crf_data.split("\n"))


def parse_nyt_recipe(html: str) -> dict:
    """return a dictionary representing a recipe from the nyt

    Args:
        html (str): the recipe page document

    Returns:
        dict: the recipe as a dict
    """
    soup = BeautifulSoup(html, "html.parser")

    name = soup.find("h1", class_="recipe-title").text.strip()
    description = soup.find("div", class_="topnote").text.strip()
    yield_tag, time_tag = [
        item for item in soup.find("ul", class_="recipe-time-yield") if item != "\n"
    ]
    servings = yield_tag.contents[3].text
    cook_time = time_tag.contents[3].text

    ingredients = [
        re.sub(r"\s{2,}", " ", item.text).strip()
        for item in soup.find("ul", class_="recipe-ingredients")
        if item != "\n"
    ]
    steps = [
        item.text for item in soup.find("ol", class_="recipe-steps") if item != "\n"
    ]

    return {
        "name": name,
        "description": description,
        "servings": servings,
        "cook_time": cook_time,
        "ingredients": ingredients,
        "steps": steps,
    }


def login_to_api():
    response = requests.get(
        f"{API_URL}/token/", files={"username": API_USER, "password": API_PASSWORD}
    )
    return response.json()
