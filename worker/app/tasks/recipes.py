import requests

from app.main import app
from app.utils import tag_ingredient_phrase


@app.task(name="app.tasks.recipes.tag_ingredients_test")
def tag_ingredients_test(ingredients: list[str]):
    return tag_ingredient_phrase(ingredients)


@app.task(name="app.tasks.recipes.tag_ingredients_in_recipe")
def tag_ingredients_in_recipe(recipe: dict):
    recipe["ingredients"] = tag_ingredient_phrase(recipe["ingredients"])
    return recipe


@app.task(name="app.tasks.recipes.post_recipe")
def post_recipe(recipe: dict):
    steps = recipe.pop("steps")
    ingredients = recipe.pop("ingredients")
