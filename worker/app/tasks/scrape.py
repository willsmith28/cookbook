import urllib.parse

import requests

from app.main import app
from app.utils import parse_nyt_recipe


@app.task(name="app.tasks.scrape.add")
def add(x, y):
    return x + y


@app.task(name="app.tasks.scrape.scrape_nyt")
def scrape_nyt(url: str):
    if urllib.parse.urlparse(url).netloc != "cooking.nytimes.com":
        raise ValueError("url must come from cooking.nytimes.com")

    response = requests.get(url)
    response.raise_for_status()
    return parse_nyt_recipe(response.text)
