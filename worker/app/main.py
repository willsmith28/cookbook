import pathlib

import celery
from app.config import CELERY_BROKER_URL, CELERY_RESULT_BACKEND


app = celery.Celery(
    __name__,
    broker=CELERY_BROKER_URL,
    backend=CELERY_RESULT_BACKEND,
    include=[
        f"app.tasks.{file.stem}"
        for file in (pathlib.Path(__file__).parent / "tasks").iterdir()
        if file.stem != "__init__"
    ],
)
