import os
import pathlib

MODEL_PATH = os.environ["MODEL_PATH"]

CELERY_BROKER_URL = os.environ["CELERY_BROKER_URL"]
CELERY_RESULT_BACKEND = os.environ["CELERY_RESULT_BACKEND"]

API_URL = os.environ["API_URL"]
API_USER = os.environ["API_USER"]
API_PASSWORD = os.environ["API_PASSWORD"]
