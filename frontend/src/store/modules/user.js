import api from "../../api";

const userMutations = {
  SET_USER: "SET_USER",
  REMOVE_USER: "REMOVE_USER",
};

export default {
  namespaced: true,
  state: () => ({
    iss: null,
    sub: null,
    username: null,
    email: null,
    active: null,
    super_user: null,
  }),
  mutations: {
    [userMutations.SET_USER]: (
      state,
      { iss, sub, username, email, active, super_user }
    ) => {
      state.iss = iss;
      state.sub = sub;
      state.username = username;
      state.email = email;
      state.active = active;
      state.super_user = super_user;
    },

    [userMutations.REMOVE_USER]: (state) => {
      state.iss = null;
      state.sub = null;
      state.username = null;
      state.email = null;
      state.active = null;
      state.super_user = null;
    },
  },
  actions: {
    async login({ commit }, { username, password }) {
      const {
        data: { accessToken },
      } = await api.tokens.login({ username, password });
      const userData = JSON.parse(atob(accessToken.split(".")[1]));

      commit(userMutations.SET_USER, userData);
    },
    async refresh() {
      await api.tokens.refresh();
    },
    async logout({ commit }) {
      await api.tokens.logout();
      commit(userMutations.REMOVE_USER);
    },
  },
  getters: {
    isLoggedIn: (state) => !!state.username,
  },
};
