import client from "./client";
import store from "../store";

const interceptor = client.interceptors.request.use(
  (request) => request,
  (error) => {
    if (error.response.status != 401) {
      return Promise.reject(error);
    }
    // eject interceptors so it doesn't loop if refresh responds with 401
    client.interceptors.response.eject(interceptor);

    const originalRequest = error.config;
    return store.dispatch("refresh").then(() => client(originalRequest));
  }
);

const api = {
  client,
  tokens: {
    login({ username, password }) {
      const loginForm = new FormData();
      loginForm.append("username", username);
      loginForm.append("password", password);
      return client.post("token/", loginForm, {
        headers: { "Content-Type": "multipart/form-data" },
      });
    },
    refresh() {
      return client.post("token/refresh/");
    },
    signUp({ username, password, email }) {
      return client.post("token/sign-up/", { username, password, email });
    },
    logout() {
      return client.post("token/logout/");
    },
  },
  ingredients: {
    getList({ pageSize = 100, prevID = null }) {
      return client.get("ingredients/", {
        params: buildParams(pageSize, prevID),
      });
    },
    create({ name, recipeId }) {
      return client.post("ingredients/", { name, recipeId });
    },
    getDetail(id) {
      return client.get(`ingredients/${id}/`);
    },
    edit({ id, name, recipeId }) {
      return client.put(`ingredients/${id}/`, { name, recipeId });
    },
    delete(id) {
      return client.delete(`ingredients/${id}/`);
    },
    getUnits() {
      return client.get("ingredients/units/");
    },
  },
  tags: {
    getList({ pageSize = 100, prevId = null }) {
      return client.get("tags/", {
        params: buildParams(pageSize, prevId),
      });
    },
    create({ name, type }) {
      return client.post("tags/", { name, type });
    },
    getDetail(id) {
      return client.get(`tags/${id}/`);
    },
    edit({ id, name, type }) {
      return client.put(`tags/${id}/`, { name, type });
    },
    delete(id) {
      return client.delete(`tags/${id}/`);
    },
    getTypes() {
      return client.get("tags/types/");
    },
  },
  recipes: {
    getList({ pageSize = 100, prevId = null }) {
      return client.get("recipes/", { params: buildParams(pageSize, prevId) });
    },
    create({
      name,
      description,
      servings,
      cookTime,
      notes,
      private: private_,
    }) {
      return client.post("recipes/", {
        name,
        description,
        servings,
        cookTime,
        notes,
        private: private_,
      });
    },
    getDetail(id) {
      return client.get(`recipes/${id}/`);
    },
    edit({
      id,
      name,
      description,
      servings,
      cookTime,
      notes,
      private: private_,
    }) {
      return client.put(`recipes/${id}/`, {
        name,
        description,
        servings,
        cookTime,
        notes,
        private: private_,
      });
    },
    delete(id) {
      return client.delete(`recipes/${id}/`);
    },
    ingredients: {
      getList(recipeId, { pageSize = 100, prevId = null }) {
        return client.get(`recipes/${recipeId}/ingredients/`, {
          params: buildParams(pageSize, prevId),
        });
      },
      create(recipeId, { amount, unit, specifier, ingredientId }) {
        return client.post(`recipes/${recipeId}/ingredients/`, {
          amount,
          unit,
          specifier,
          ingredientId,
        });
      },
      getDetail(recipeId, ingredientId) {
        return client.get(`recipes/${recipeId}/ingredients/${ingredientId}/`);
      },
      edit(recipeId, { id, amount, unit, specifier, ingredientId }) {
        return client.put(`recipes/${recipeId}/ingredients/${id}/`, {
          amount,
          unit,
          specifier,
          ingredientId,
        });
      },
      delete(recipeId, ingredientId) {
        return client.delete(
          `recipes/${recipeId}/ingredients/${ingredientId}/`
        );
      },
    },
    steps: {
      getList(recipeId, { pageSize = 100, prevId = null }) {
        return client.get(`recipes/${recipeId}/steps/`, {
          params: buildParams(pageSize, prevId),
        });
      },
      create(recipeId, { instruction }) {
        return client.post(`recipes/${recipeId}/steps/`, {
          instruction,
        });
      },
      getDetail(recipeId, order) {
        return client.get(`recipes/${recipeId}/steps/${order}/`);
      },
      edit(recipeId, { order, instruction }) {
        return client.put(`recipes/${recipeId}/steps/${order}/`, {
          instruction,
        });
      },
      delete(recipeId, order) {
        return client.delete(`recipes/${recipeId}/steps/${order}/`);
      },
    },
    tags: {
      getList(recipeId, { pageSize = 100, prevId = null }) {
        return client.get(`recipes/${recipeId}/tags/`, {
          params: buildParams(pageSize, prevId),
        });
      },
      create(recipeId, tagId) {
        return client.post(`recipes/${recipeId}/tags/`, {
          id: tagId,
        });
      },
      delete(recipeId, tagId) {
        return client.delete(`recipes/${recipeId}/tags/${tagId}/`);
      },
    },
  },
};

export default api;

function buildParams(pageSize = 100, prevID = null) {
  const params = {};
  if (pageSize) {
    params.page_size = pageSize;
  }
  if (prevID) {
    params.prev_id = prevID;
  }
  return params;
}
