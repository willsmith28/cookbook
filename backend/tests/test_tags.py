import pytest
from httpx import AsyncClient
from sqlalchemy import func, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload

from app.constants import DEFAULT_PAGE_SIZE
from app.database import models
from tests import TEST_USER_EMAIL, fake, random_tag

# ########
# # /tags/
# ########


@pytest.mark.asyncio
async def test_get_tag_list_default_page_size(
    dataset: AsyncSession, async_client: AsyncClient
):
    """GET /tags/"""
    session = dataset
    response = await async_client.get("/tags/", params={"page_size": DEFAULT_PAGE_SIZE})
    assert response.status_code == 200
    result = await session.execute(select(func.count("*")).select_from(models.Tag))
    response_json = response.json()
    assert (
        len(response_json["items"]) <= DEFAULT_PAGE_SIZE
        and DEFAULT_PAGE_SIZE == response_json["pageSize"]
        and response_json["totalItems"] == result.scalar()
    )


@pytest.mark.asyncio
async def test_get_tag_list_diffrent_page_size(
    dataset: AsyncSession, async_client: AsyncClient
):
    """GET /tags/"""
    session = dataset
    page_size = fake.pyint()
    response = await async_client.get("/tags/", params={"page_size": page_size})
    assert response.status_code == 200
    result = await session.execute(select(func.count("*")).select_from(models.Tag))
    response_json = response.json()
    assert (
        len(response_json["items"]) <= page_size
        and page_size == response_json["pageSize"]
        and response_json["totalItems"] == result.scalar()
    )


@pytest.mark.asyncio
async def test_get_tag_list_all_pages(dataset: AsyncSession, async_client: AsyncClient):
    """GET /tags/"""
    session = dataset
    response = await async_client.get("/tags/", params={"page_size": DEFAULT_PAGE_SIZE})
    assert response.status_code == 200
    response_json = response.json()
    result = await session.execute(select(func.count("*")).select_from(models.Tag))
    total_items_db = result.scalar()
    total_items = response_json["totalItems"]
    assert total_items_db == total_items

    items_seen = len(response_json["items"])
    while len(response_json["items"]) == DEFAULT_PAGE_SIZE:
        prev_id = response_json["items"][-1]["id"]
        response = await async_client.get(
            "/tags/", params={"page_size": DEFAULT_PAGE_SIZE, "prev_id": prev_id}
        )
        response_json = response.json()
        items_seen += len(response_json["items"])

    assert items_seen == total_items


@pytest.mark.asyncio
async def test_tag_post(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """
    POST /tag/
    """
    session = dataset
    response = await async_client.post(
        "/tags/",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
        json=random_tag(),
    )
    assert response.status_code == 201
    result = await session.execute(
        select(models.Tag).filter_by(id=response.json()["id"])
    )
    tag = result.scalar()
    assert tag is not None


@pytest.mark.asyncio
async def test_tag_post_unauthorized(dataset: AsyncSession, async_client: AsyncClient):
    response = await async_client.post(
        "/tags/",
        json=random_tag(),
    )
    assert response.status_code == 401


@pytest.mark.asyncio
async def test_tag_post_already_exists(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """
    Post /tag/ already exists
    """
    session = dataset
    result = await session.execute(select(models.Tag))
    tag = result.scalars().first()
    new_tag = random_tag()
    new_tag["name"] = tag.name
    response = await async_client.post(
        "/tags/",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
        json=new_tag,
    )
    assert response.status_code == 409


@pytest.mark.asyncio
async def test_get_tag_detail(dataset: AsyncSession, async_client: AsyncClient):
    """
    GET /tag/<int:pk>/
    """
    session = dataset
    result = await session.execute(select(models.Tag))
    tag = result.scalars().first()
    response = await async_client.get(f"/tags/{tag.id}/")
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_edit_tag_owner(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """PUT /tag/pk/"""
    session = dataset
    result = await session.execute(select(models.Tag))
    tag = result.scalars().first()
    tag_id = tag.id
    changed_tag = random_tag()
    response = await async_client.put(
        f"/tags/{tag_id}",
        json=changed_tag,
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
    )
    result = await session.execute(select(models.Tag).where(models.Tag.id == tag_id))
    tag = result.scalars().first()
    assert response.status_code == 200 and changed_tag["name"] == tag.name


@pytest.mark.asyncio
async def test_edit_tag_nonowner(
    dataset: AsyncSession, async_client: AsyncClient, non_owner_user_token: str
):
    """PUT /tag/pk/"""
    session = dataset
    result = await session.execute(select(models.Tag))
    tag = result.scalars().first()
    response = await async_client.put(
        f"/tags/{tag.id}",
        json=random_tag(),
        headers={"Authorization": f"Bearer {non_owner_user_token}"},
    )
    assert response.status_code == 403


@pytest.mark.asyncio
async def test_edit_tag_superuser(
    dataset: AsyncSession, async_client: AsyncClient, super_user_token: str
):
    """PUT /tag/pk/"""
    session = dataset
    result = await session.execute(select(models.Tag))
    tag = result.scalars().first()
    tag_id = tag.id
    changed_tag = random_tag()
    response = await async_client.put(
        f"/tags/{tag_id}",
        json=changed_tag,
        headers={"Authorization": f"Bearer {super_user_token}"},
    )
    result = await session.execute(select(models.Tag).where(models.Tag.id == tag_id))
    tag = result.scalars().first()
    assert response.status_code == 200 and tag.name == changed_tag["name"]


@pytest.mark.asyncio
async def test_delete_tag_owner(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """DELETE /tag/pk/"""
    session = dataset
    result = await session.execute(
        select(models.User).where(models.User.email == TEST_USER_EMAIL)
    )
    user = result.scalar()
    db_tag = models.Tag(**random_tag(), created_by_id=user.id)
    session.add(db_tag)
    await session.flush()
    tag_id = db_tag.id
    response = await async_client.delete(
        f"/tags/{tag_id}",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
    )
    result = await session.execute(select(models.Tag).where(models.Tag.id == tag_id))
    tag = result.scalars().first()
    assert response.status_code == 204 and tag is None


@pytest.mark.asyncio
async def test_delete_tag_nonowner(
    dataset: AsyncSession, async_client: AsyncClient, non_owner_user_token: str
):
    """DELETE /tag/pk/"""
    session = dataset
    result = await session.execute(select(models.Tag))
    tag = result.scalars().first()
    tag_id = tag.id
    response = await async_client.delete(
        f"/tags/{tag.id}",
        headers={"Authorization": f"Bearer {non_owner_user_token}"},
    )
    result = await session.execute(select(models.Tag).where(models.Tag.id == tag_id))
    tag = result.scalars().first()
    assert response.status_code == 403 and tag is not None


@pytest.mark.asyncio
async def test_delete_tag_superuser(
    dataset: AsyncSession, async_client: AsyncClient, super_user_token: str
):
    """DELETE /tag/pk/"""
    session = dataset
    result = await session.execute(
        select(models.User).where(models.User.email == TEST_USER_EMAIL)
    )
    user = result.scalar()
    db_tag = models.Tag(**random_tag(), created_by_id=user.id)
    session.add(db_tag)
    await session.flush()
    tag_id = db_tag.id
    response = await async_client.delete(
        f"/tags/{tag_id}",
        headers={"Authorization": f"Bearer {super_user_token}"},
    )
    result = await session.execute(select(models.Tag).where(models.Tag.id == tag_id))
    tag = result.scalars().first()
    assert response.status_code == 204 and tag is None


@pytest.mark.asyncio
async def test_delete_tag_in_recipe(
    dataset: AsyncSession, async_client: AsyncClient, super_user_token: str
):
    """DELETE /tag/pk/"""
    session = dataset
    query = select(models.Recipe).options(selectinload(models.Recipe.tags))
    result = await session.execute(query)
    recipe = result.scalars().first()
    tag_id = recipe.tags[0].id
    response = await async_client.delete(
        f"/tags/{tag_id}",
        headers={"Authorization": f"Bearer {super_user_token}"},
    )
    result = await session.execute(select(models.Tag).where(models.Tag.id == tag_id))
    tag = result.scalars().first()
    assert response.status_code == 409 and tag is not None
