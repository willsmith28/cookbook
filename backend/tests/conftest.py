import asyncio
import contextlib
import random

import psycopg2
import pytest
from httpx import AsyncClient
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import selectinload, sessionmaker

from app.auth import get_password_hash
from app.config import DB, HOST, PASSWORD, PORT, USER
from app.database import get_database_session, models
from app.main import app
from tests import (
    TEST_SUPER_EMAIL,
    TEST_SUPER_USERNAME,
    TEST_USER1_EMAIL,
    TEST_USER1_USERNAME,
    TEST_USER_EMAIL,
    TEST_USER_PASS,
    TEST_USER_USERNAME,
    random_ingredient,
    random_ingredient_in_recipe,
    random_meal_plan,
    random_recipe,
    random_step,
    random_tag,
)

TEST_DB = f"{DB}_test"
ASYNC_TEST_DB_URL = f"postgresql+asyncpg://{USER}:{PASSWORD}@{HOST}:{PORT}/{TEST_DB}"


@pytest.fixture(scope="session")
def event_loop():
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="function")
async def logged_in_user_token(async_client: AsyncClient) -> str:
    response = await async_client.post(
        "/token/", data={"username": TEST_USER_USERNAME, "password": TEST_USER_PASS}
    )
    return response.json()["accessToken"]


@pytest.fixture(scope="function")
async def non_owner_user_token(async_client: AsyncClient) -> str:
    response = await async_client.post(
        "/token/", data={"username": TEST_USER1_USERNAME, "password": TEST_USER_PASS}
    )
    return response.json()["accessToken"]


@pytest.fixture(scope="function")
async def super_user_token(async_client: AsyncClient) -> str:
    response = await async_client.post(
        "/token/", data={"username": TEST_SUPER_USERNAME, "password": TEST_USER_PASS}
    )
    return response.json()["accessToken"]


@pytest.fixture(scope="session")
async def setup_test_engine():
    dsn = f"host={HOST} dbname=postgres user={USER} password={PASSWORD}"
    with contextlib.closing(psycopg2.connect(dsn)) as connection:
        connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"CREATE DATABASE {TEST_DB};")

        except psycopg2.ProgrammingError:
            # if duplicate database do nothing
            pass

    test_engine = create_async_engine(ASYNC_TEST_DB_URL)

    try:

        # setup test db
        async with test_engine.begin() as connection:
            await connection.run_sync(models.mapper_registry.metadata.drop_all)
            await connection.run_sync(models.mapper_registry.metadata.create_all)

        yield test_engine

        await test_engine.dispose()

    finally:
        with contextlib.closing(psycopg2.connect(dsn)) as connection:
            connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
            with connection.cursor() as cursor:
                cursor.execute(f"DROP DATABASE {TEST_DB}")


@pytest.fixture(scope="session")
async def setup_async_test_db_connection(setup_test_engine):
    """connects to test database creates all tables then cleans up after

    Yields:
        Iterator[Database]: SQLAlchemy connection
    """
    Session = sessionmaker(
        setup_test_engine, class_=AsyncSession, expire_on_commit=False
    )
    # setup test db

    return Session


@pytest.fixture(scope="session")
async def insert_test_data(
    setup_async_test_db_connection,
):
    Session = setup_async_test_db_connection

    session: AsyncSession
    async with Session.begin() as session:
        super_user = models.User(
            username=TEST_SUPER_USERNAME,
            email=TEST_SUPER_EMAIL,
            hashed_password=get_password_hash(TEST_USER_PASS),
            super_user=True,
        )
        user = models.User(
            username=TEST_USER_USERNAME,
            email=TEST_USER_EMAIL,
            hashed_password=get_password_hash(TEST_USER_PASS),
        )
        user1 = models.User(
            username=TEST_USER1_USERNAME,
            email=TEST_USER1_EMAIL,
            hashed_password=get_password_hash(TEST_USER_PASS),
        )
        session.add_all([user, super_user, user1])
        await session.flush()

        session.add_all(
            [
                models.Ingredient(
                    **random_ingredient(),
                    recipe_id=None,
                    created_by_id=user.id,
                )
                for _ in range(500)
            ]
        )
        recipes = [
            models.Recipe(**random_recipe(), created_by_id=user.id) for _ in range(300)
        ]
        session.add_all(recipes)
        tags = [models.Tag(**random_tag(), created_by_id=user.id) for _ in range(240)]
        session.add_all(tags)
        ingredients = [
            models.Ingredient(
                recipe_id=recipe.id,
                name=f"{recipe.name} ingredient",
                created_by_id=user.id,
            )
            for recipe in set(random.choices(recipes, k=25))
        ]
        session.add_all(ingredients)

        query = select(models.Recipe).options(
            selectinload(models.Recipe.tags),
            selectinload(models.Recipe.steps),
            selectinload(models.Recipe.ingredients),
        )
        result = await session.execute(query)
        recipes: list[models.Recipe] = result.scalars().fetchall()

        for recipe in recipes:
            recipe.steps.extend(
                models.Step(
                    **random_step(),
                    recipe_id=recipe.id,
                    order=j,
                    created_by_id=user.id,
                )
                for j in range(1, random.randint(4, 11))
            )
            recipe.tags.extend(
                tag for tag in set(random.choices(tags, k=random.randint(3, 15)))
            )
            recipe.ingredients.extend(
                models.IngredientInRecipe(
                    **random_ingredient_in_recipe(),
                    recipe_id=recipe.id,
                    ingredient_id=ingredient_id,
                    created_by_id=user.id,
                )
                for ingredient_id in {
                    ingredient.id
                    for ingredient in random.choices(
                        ingredients, k=random.randint(5, 20)
                    )
                }
            )

        session.add_all(
            [
                models.MealPlan(
                    **random_meal_plan(), recipe_id=recipe_id, created_by_id=user.id
                )
                for recipe_id in {
                    recipe.id
                    for recipe in random.choices(recipes, k=random.randint(5, 20))
                }
            ]
        )

    return Session


@pytest.fixture(scope="function")
async def dataset(insert_test_data):
    Session = insert_test_data
    session: AsyncSession
    async with Session() as session:
        await session.begin_nested()

        yield session

        await session.rollback()


@pytest.fixture(scope="function")
async def async_client(dataset):
    session = dataset

    async def override_get_db_connection():
        yield session

    app.dependency_overrides[get_database_session] = override_get_db_connection

    async with AsyncClient(
        app=app, base_url="http://test", follow_redirects=True
    ) as ac:
        yield ac
