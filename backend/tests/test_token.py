import pytest
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession

from tests import TEST_USER_PASS, TEST_USER_USERNAME, random_user

#########
# /token/
#########


@pytest.mark.asyncio
async def test_login(dataset: AsyncSession, async_client: AsyncClient):
    """POST /token/"""
    response = await async_client.post(
        "/token/", data={"username": TEST_USER_USERNAME, "password": TEST_USER_PASS}
    )
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_sign_up(dataset: AsyncSession, async_client: AsyncClient):
    response = await async_client.post("/token/sign-up/", json=random_user())
    assert response.status_code == 201
