import pytest
from httpx import AsyncClient
from sqlalchemy import func, select
from sqlalchemy.ext.asyncio import AsyncSession

from app.constants import DEFAULT_PAGE_SIZE
from app.database import models
from tests import TEST_USER_USERNAME, random_meal_plan


###############
# # /meal-plans
###############
@pytest.mark.asyncio
async def test_get_meal_plan_list(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """GET /meal-plans"""
    session = dataset
    response = await async_client.get(
        "/meal-plans/",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
    )
    result = await session.execute(
        select(models.User).where(models.User.username == TEST_USER_USERNAME)
    )
    user = result.scalar()
    result = await session.execute(
        select(func.count("*"))
        .select_from(models.MealPlan)
        .where(models.MealPlan.created_by_id == user.id)
    )
    response_json = response.json()
    assert (
        len(response_json["items"]) <= DEFAULT_PAGE_SIZE
        and DEFAULT_PAGE_SIZE == response_json["pageSize"]
        and response_json["totalItems"] == result.scalar()
    )


@pytest.mark.asyncio
async def test_get_meal_plan_unauthorized(
    dataset: AsyncSession, async_client: AsyncClient
):
    """GET /meal-plans"""
    response = await async_client.get(
        "/meal-plans/",
    )

    assert response.status_code == 401


@pytest.mark.asyncio
async def test_create_meal_plan(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """POST /meal-plan"""
    session = dataset
    result = await session.execute(select(models.Recipe))
    recipe = result.scalars().first()
    meal_plan = random_meal_plan()
    meal_plan.update({"date": str(meal_plan["date"]), "recipe_id": str(recipe.id)})
    response = await async_client.post(
        "/meal-plans/",
        json=meal_plan,
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
    )
    assert response.status_code == 201


@pytest.mark.asyncio
async def test_post_meal_plan_unauthorized(
    dataset: AsyncSession,
    async_client: AsyncClient,
):
    session = dataset
    result = await session.execute(select(models.Recipe))
    recipe = result.scalars().first()
    meal_plan = random_meal_plan()
    meal_plan.update({"date": str(meal_plan["date"]), "recipe_id": str(recipe.id)})
    response = await async_client.post(
        "/meal-plans/",
        json=meal_plan,
    )
    assert response.status_code == 401


########################
# /meal-plans/{id: uuid}
########################


@pytest.mark.asyncio
async def test_get_meal_plan_detail(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """GET /meal_plan/pk/"""
    session = dataset
    result = await session.execute(select(models.MealPlan))
    meal_plan = result.scalars().first()
    response = await async_client.get(
        f"/meal-plans/{meal_plan.id}/",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
    )
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_edit_meal_plan_owner(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """PUT /meal_plan/pk/"""
    session = dataset
    meal_plan = random_meal_plan()
    meal_plan.update({"date": str(meal_plan["date"])})
    result = await session.execute(select(models.MealPlan))
    db_meal_plan = result.scalars().first()
    response = await async_client.put(
        f"/meal-plans/{db_meal_plan.id}",
        json=meal_plan,
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
    )
    result = await session.execute(
        select(models.MealPlan).where(models.MealPlan.id == db_meal_plan.id)
    )
    db_meal_plan = result.scalars().first()
    assert response.status_code == 200 and meal_plan["type"] == db_meal_plan.type


@pytest.mark.asyncio
async def test_edit_meal_plan_nonowner(
    dataset: AsyncSession, async_client: AsyncClient, non_owner_user_token: str
):
    session = dataset
    meal_plan = random_meal_plan()
    meal_plan.update({"date": str(meal_plan["date"])})
    result = await session.execute(select(models.MealPlan))
    db_meal_plan = result.scalars().first()
    response = await async_client.put(
        f"/meal-plans/{db_meal_plan.id}",
        json=meal_plan,
        headers={"Authorization": f"Bearer {non_owner_user_token}"},
    )
    assert response.status_code == 403


@pytest.mark.asyncio
async def test_edit_meal_plan_superuser(
    dataset: AsyncSession, async_client: AsyncClient, super_user_token: str
):
    """PUT /meal_plan/pk/"""
    session = dataset
    meal_plan = random_meal_plan()
    meal_plan.update({"date": str(meal_plan["date"])})
    result = await session.execute(select(models.MealPlan))
    db_meal_plan = result.scalars().first()
    response = await async_client.put(
        f"/meal-plans/{db_meal_plan.id}",
        json=meal_plan,
        headers={"Authorization": f"Bearer {super_user_token}"},
    )
    result = await session.execute(
        select(models.MealPlan).where(models.MealPlan.id == db_meal_plan.id)
    )
    db_meal_plan = result.scalars().first()
    assert response.status_code == 200 and meal_plan["type"] == db_meal_plan.type


@pytest.mark.asyncio
async def test_delete_meal_plan_owner(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """DELETE /meal_plan/pk/"""
    session = dataset
    result = await session.execute(select(models.MealPlan))
    meal_plan = result.scalars().first()
    meal_plan_id = meal_plan.id
    response = await async_client.delete(
        f"/meal-plans/{meal_plan.id}",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
    )
    result = await session.execute(
        select(models.MealPlan).where(models.MealPlan.id == meal_plan_id)
    )
    meal_plan = result.scalars().first()
    assert response.status_code == 204 and meal_plan is None


@pytest.mark.asyncio
async def test_delete_meal_plan_nonowner(
    dataset: AsyncSession, async_client: AsyncClient, non_owner_user_token: str
):
    """DELETE /meal_plan/pk/"""
    session = dataset
    result = await session.execute(select(models.MealPlan))
    meal_plan = result.scalars().first()
    meal_plan_id = meal_plan.id
    response = await async_client.delete(
        f"/meal-plans/{meal_plan.id}",
        headers={"Authorization": f"Bearer {non_owner_user_token}"},
    )
    result = await session.execute(
        select(models.MealPlan).where(models.MealPlan.id == meal_plan_id)
    )
    meal_plan = result.scalars().first()
    assert response.status_code == 403 and meal_plan is not None


@pytest.mark.asyncio
async def test_delete_meal_plan_superuser(
    dataset: AsyncSession, async_client: AsyncClient, super_user_token: str
):
    """DELETE /meal_plan/pk/"""
    session = dataset
    result = await session.execute(select(models.MealPlan))
    meal_plan = result.scalars().first()
    meal_plan_id = meal_plan.id
    response = await async_client.delete(
        f"/meal-plans/{meal_plan.id}",
        headers={"Authorization": f"Bearer {super_user_token}"},
    )
    result = await session.execute(
        select(models.MealPlan).where(models.MealPlan.id == meal_plan_id)
    )
    meal_plan = result.scalars().first()
    assert response.status_code == 204 and meal_plan is None
