import random

from faker import Faker
from fastapi.testclient import TestClient

from app import constants
from app.main import app

TEST_SUPER_USERNAME = "superMcSuperson"
TEST_SUPER_EMAIL = "super@super.com"
TEST_USER_USERNAME = "testerMcTesterson"
TEST_USER_EMAIL = "test@test.com"
TEST_USER1_USERNAME = "testerMcTesterson1"
TEST_USER1_EMAIL = "test1@test.com"
TEST_USER_PASS = "test"


fake = Faker()


client = TestClient(app)


def random_recipe() -> dict:
    return {
        "name": fake.unique.name(),
        "description": fake.paragraph(),
        "servings": fake.sentence(),
        "cook_time": fake.sentence(),
        "private": fake.pybool(),
    }


def random_ingredient() -> dict:
    return {"name": fake.unique.name()}


def random_ingredient_in_recipe() -> dict:
    return {
        "amount": random.choice(
            (str(fake.pydecimal(left_digits=4, right_digits=2, positive=True)),)
        ),
        "unit": random.choice((*constants.IngredientUnits, None)),
        "comment": random.choice((fake.word(), None)),
        "other": random.choice((fake.word(), None)),
    }


def random_tag() -> dict:
    return {
        "name": fake.unique.word(),
        "type": random.choice((*constants.TagTypes,)),
    }


def random_step() -> dict:
    return {"instruction": fake.paragraph()}


def random_user() -> dict:
    return {
        "username": fake.unique.name(),
        "email": fake.unique.email(),
        "password": fake.password(),
    }


def random_meal_plan() -> dict:
    return {
        "date": fake.future_date(),
        "type": random.choice((*constants.MealPlanTypes,)),
        "cooked": fake.boolean(),
    }
