import pytest
from httpx import AsyncClient
from sqlalchemy import func, or_, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload

from app.constants import DEFAULT_PAGE_SIZE
from app.database import models
from tests import (
    TEST_USER_USERNAME,
    random_ingredient,
    random_ingredient_in_recipe,
    random_recipe,
    random_step,
    random_tag,
)


#############
# # /recipes/
#############
@pytest.mark.asyncio
async def test_get_recipe_list_no_user(
    dataset: AsyncSession, async_client: AsyncClient
):
    """GET /recipes/"""
    session = dataset
    response = await async_client.get(
        "/recipes/", params={"page_size": DEFAULT_PAGE_SIZE}
    )
    assert response.status_code == 200
    query = (
        select(func.count("*"))
        .select_from(models.Recipe)
        .where(models.Recipe.private.is_(False))
    )
    result = await session.execute(query)
    response_json = response.json()
    assert (
        len(response_json["items"]) <= DEFAULT_PAGE_SIZE
        and DEFAULT_PAGE_SIZE == response_json["pageSize"]
        and response_json["totalItems"] == result.scalar()
    )


@pytest.mark.asyncio
async def test_get_recipe_list_logged_in(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """GET /recipes/"""
    session = dataset
    response = await async_client.get(
        "/recipes/",
        params={"page_size": DEFAULT_PAGE_SIZE},
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
    )
    assert response.status_code == 200

    result = await session.execute(
        select(models.User).where(models.User.username == TEST_USER_USERNAME)
    )
    user = result.scalar()
    query = (
        select(func.count("*"))
        .select_from(models.Recipe)
        .where(
            or_(
                models.Recipe.private.is_(False), models.Recipe.created_by_id == user.id
            )
        )
    )
    result = await session.execute(query)
    response_json = response.json()
    assert (
        len(response_json["items"]) <= DEFAULT_PAGE_SIZE
        and DEFAULT_PAGE_SIZE == response_json["pageSize"]
        and response_json["totalItems"] == result.scalar()
    )


@pytest.mark.asyncio
async def test_get_recipe_list_superuser(
    dataset: AsyncSession, async_client: AsyncClient, super_user_token: str
):
    """GET /recipes/"""
    session = dataset
    response = await async_client.get(
        "/recipes/",
        params={"page_size": DEFAULT_PAGE_SIZE},
        headers={"Authorization": f"Bearer {super_user_token}"},
    )
    assert response.status_code == 200
    query = select(func.count("*")).select_from(models.Recipe)
    result = await session.execute(query)
    response_json = response.json()
    assert (
        len(response_json["items"]) <= DEFAULT_PAGE_SIZE
        and DEFAULT_PAGE_SIZE == response_json["pageSize"]
        and response_json["totalItems"] == result.scalar()
    )


@pytest.mark.asyncio
async def test_recipe_post(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """POST /recipes/"""
    session = dataset
    response = await async_client.post(
        "/recipes/",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
        json=random_recipe(),
    )
    assert response.status_code == 201
    query = select(models.Recipe).where(models.Recipe.id == response.json()["id"])
    recipe = await session.execute(query)
    assert recipe is not None


@pytest.mark.asyncio
async def test_recipe_post_unauthorized(
    dataset: AsyncSession, async_client: AsyncClient
):
    response = await async_client.post(
        "/recipes/",
        json=random_recipe(),
    )
    assert response.status_code == 401


######################
# # /recipes/{id: int}
######################
@pytest.mark.asyncio
async def test_get_recipe_detail_unauthorized(
    dataset: AsyncSession, async_client: AsyncClient
):
    """GET /recipes/{id: int}"""
    session = dataset
    result = await session.execute(
        select(models.Recipe).where(models.Recipe.private.is_(False))
    )
    recipe = result.scalars().first()
    response = await async_client.get(f"/recipes/{recipe.id}/")
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_get_recipe_detail_private(
    dataset: AsyncSession, async_client: AsyncClient
):
    """GET /recipes/{id: int}"""
    session = dataset
    result = await session.execute(
        select(models.Recipe).where(models.Recipe.private.is_(True))
    )
    recipe = result.scalars().first()
    response = await async_client.get(f"/recipes/{recipe.id}/")
    assert response.status_code == 404


@pytest.mark.asyncio
async def test_get_recipe_detail_private_non_owner(
    dataset: AsyncSession, async_client: AsyncClient, non_owner_user_token: str
):
    """GET /recipes/{id: int}"""
    session = dataset
    result = await session.execute(
        select(models.Recipe).where(models.Recipe.private.is_(True))
    )
    recipe = result.scalars().first()
    response = await async_client.get(
        f"/recipes/{recipe.id}/",
        headers={"Authorization": f"Bearer {non_owner_user_token}"},
    )
    assert response.status_code == 404


@pytest.mark.asyncio
async def test_get_recipe_detail_private_super_user(
    dataset: AsyncSession, async_client: AsyncClient, super_user_token: str
):
    """GET /recipes/{id: int}"""
    session = dataset
    result = await session.execute(
        select(models.Recipe).where(models.Recipe.private.is_(True))
    )
    recipe = result.scalars().first()
    response = await async_client.get(
        f"/recipes/{recipe.id}/",
        headers={"Authorization": f"Bearer {super_user_token}"},
    )
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_edit_recipe(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """PUT /recipes/{id: int}"""
    session = dataset
    result = await session.execute(select(models.Recipe))
    recipe = result.scalars().first()
    changed_recipe = random_recipe()
    response = await async_client.put(
        f"/recipes/{recipe.id}/",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
        json=changed_recipe,
    )
    result = await session.execute(
        select(models.Recipe).where(models.Recipe.id == recipe.id)
    )
    recipe = result.scalar()
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_super_user_edit_recipe(
    dataset: AsyncSession, async_client: AsyncClient, super_user_token: str
):
    """PUT /recipes/{id: int}"""
    session = dataset
    result = await session.execute(select(models.Recipe))
    recipe = result.scalars().first()
    changed_recipe = random_recipe()
    response = await async_client.put(
        f"/recipes/{recipe.id}/",
        headers={"Authorization": f"Bearer {super_user_token}"},
        json=changed_recipe,
    )
    result = await session.execute(
        select(models.Recipe).where(models.Recipe.id == recipe.id)
    )
    recipe = result.scalar()
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_non_owner_edit_recipe(
    dataset: AsyncSession, async_client: AsyncClient, non_owner_user_token: str
):
    """PUT /recipes/{id: int}"""
    session = dataset
    result = await session.execute(
        select(models.Recipe).where(models.Recipe.private.is_(False))
    )
    recipe = result.scalars().first()
    changed_recipe = random_recipe()
    response = await async_client.put(
        f"/recipes/{recipe.id}/",
        headers={"Authorization": f"Bearer {non_owner_user_token}"},
        json=changed_recipe,
    )
    assert response.status_code == 403


@pytest.mark.asyncio
async def test_non_owner_edit_private_recipe(
    dataset: AsyncSession, async_client: AsyncClient, non_owner_user_token: str
):
    """PUT /recipes/{id: int}"""
    session = dataset
    result = await session.execute(
        select(models.Recipe).where(models.Recipe.private.is_(True))
    )
    recipe = result.scalars().first()
    changed_recipe = random_recipe()
    response = await async_client.put(
        f"/recipes/{recipe.id}/",
        headers={"Authorization": f"Bearer {non_owner_user_token}"},
        json=changed_recipe,
    )
    assert response.status_code == 404


@pytest.mark.asyncio
async def test_delete_recipe(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    session = dataset
    result = await session.execute(select(models.Recipe))
    recipe = result.scalars().first()
    recipe_id = recipe.id
    response = await async_client.delete(
        f"/recipes/{recipe_id}/",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
    )
    result = await session.execute(
        select(models.Recipe).where(models.Recipe.id == recipe_id)
    )
    recipe = result.scalar()
    assert response.status_code == 204 and recipe is None


@pytest.mark.asyncio
async def test_delete_recipe_super_user(
    dataset: AsyncSession, async_client: AsyncClient, super_user_token: str
):
    session = dataset
    result = await session.execute(select(models.Recipe))
    recipe = result.scalars().first()
    recipe_id = recipe.id
    response = await async_client.delete(
        f"/recipes/{recipe_id}/",
        headers={"Authorization": f"Bearer {super_user_token}"},
    )
    result = await session.execute(
        select(models.Recipe).where(models.Recipe.id == recipe_id)
    )
    recipe = result.scalar()
    assert response.status_code == 204 and recipe is None


@pytest.mark.asyncio
async def test_delete_recipe_non_owner(
    dataset: AsyncSession, async_client: AsyncClient, non_owner_user_token: str
):
    session = dataset
    result = await session.execute(
        select(models.Recipe).where(models.Recipe.private.is_(False))
    )
    recipe = result.scalars().first()
    recipe_id = recipe.id
    response = await async_client.delete(
        f"/recipes/{recipe_id}/",
        headers={"Authorization": f"Bearer {non_owner_user_token}"},
    )
    result = await session.execute(
        select(models.Recipe).where(models.Recipe.id == recipe_id)
    )
    recipe = result.scalar()
    assert response.status_code == 403 and recipe is not None


@pytest.mark.asyncio
async def test_edit_recipe_unauthorized(
    dataset: AsyncSession, async_client: AsyncClient
):
    session = dataset
    result = await session.execute(select(models.Recipe))
    recipe = result.scalars().first()
    changed_recipe = random_recipe()
    response = await async_client.put(
        f"/recipes/{recipe.id}/",
        json=changed_recipe,
    )
    assert response.status_code == 401


###################################
# # /recipes/{id: int}/ingredients/
###################################
@pytest.mark.asyncio
async def test_get_recipe_ingredients(dataset: AsyncSession, async_client: AsyncClient):
    """GET /recipes/{id: int}/ingredients/"""
    session = dataset
    result = await session.execute(
        select(models.Recipe).where(models.Recipe.private.is_(False))
    )
    recipe = result.scalars().first()
    result = await session.execute(
        select(func.count())
        .select_from(models.IngredientInRecipe)
        .where(models.IngredientInRecipe.recipe_id == recipe.id)
    )
    response = await async_client.get(
        f"/recipes/{recipe.id}/ingredients/", params={"page_size": DEFAULT_PAGE_SIZE}
    )
    response_json = response.json()
    assert (
        len(response_json["items"]) <= DEFAULT_PAGE_SIZE
        and DEFAULT_PAGE_SIZE == response_json["pageSize"]
        and response_json["totalItems"] == result.scalar()
    )


@pytest.mark.asyncio
async def test_add_recipe_ingredient(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """POST /recipes/{id: int}/ingredients/"""
    session = dataset
    result = await session.execute(select(models.User))
    user = result.scalars().first()
    ingredient = models.Ingredient(**random_ingredient(), created_by_id=user.id)
    session.add(ingredient)
    result = await session.execute(select(models.Recipe))
    recipe = result.scalars().first()
    await session.commit()
    response = await async_client.post(
        f"/recipes/{recipe.id}/ingredients/",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
        json={
            **random_ingredient_in_recipe(),
            "ingredient_id": str(ingredient.id),
        },
    )
    print(response.content)
    assert response.status_code == 201


@pytest.mark.asyncio
async def test_super_user_add_recipe_ingredient(
    dataset: AsyncSession, async_client: AsyncClient, super_user_token: str
):
    """POST /recipes/{id: int}/ingredients/"""
    session = dataset
    result = await session.execute(select(models.User))
    user = result.scalars().first()
    ingredient = models.Ingredient(**random_ingredient(), created_by_id=user.id)
    session.add(ingredient)
    result = await session.execute(select(models.Recipe))
    recipe = result.scalars().first()
    await session.commit()
    response = await async_client.post(
        f"/recipes/{recipe.id}/ingredients/",
        headers={"Authorization": f"Bearer {super_user_token}"},
        json={
            **random_ingredient_in_recipe(),
            "ingredient_id": str(ingredient.id),
        },
    )
    assert response.status_code == 201


@pytest.mark.asyncio
async def test_non_owner_add_recipe_ingredient(
    dataset: AsyncSession, async_client: AsyncClient, non_owner_user_token: str
):
    session = dataset
    result = await session.execute(select(models.User))
    user = result.scalars().first()
    ingredient = models.Ingredient(**random_ingredient(), created_by_id=user.id)
    session.add(ingredient)
    result = await session.execute(
        select(models.Recipe).where(models.Recipe.private.is_(False))
    )
    recipe = result.scalars().first()

    response = await async_client.post(
        f"/recipes/{recipe.id}/ingredients/",
        headers={"Authorization": f"Bearer {non_owner_user_token}"},
        json={
            **random_ingredient_in_recipe(),
            "ingredient_id": str(ingredient.id),
        },
    )
    assert response.status_code == 403


@pytest.mark.asyncio
async def test_non_owner_add_recipe_ingredient_private_recipe(
    dataset: AsyncSession, async_client: AsyncClient, non_owner_user_token: str
):
    session = dataset
    result = await session.execute(select(models.User))
    user = result.scalars().first()
    ingredient = models.Ingredient(**random_ingredient(), created_by_id=user.id)
    session.add(ingredient)
    result = await session.execute(
        select(models.Recipe).where(models.Recipe.private.is_(True))
    )
    recipe = result.scalars().first()

    response = await async_client.post(
        f"/recipes/{recipe.id}/ingredients/",
        headers={"Authorization": f"Bearer {non_owner_user_token}"},
        json={
            **random_ingredient_in_recipe(),
            "ingredient_id": str(ingredient.id),
        },
    )
    assert response.status_code == 404


@pytest.mark.asyncio
async def test_add_recipe_ingredient_unauthorized(
    dataset: AsyncSession, async_client: AsyncClient
):
    session = dataset
    result = await session.execute(select(models.User))
    user = result.scalars().first()
    ingredient = models.Ingredient(**random_ingredient(), created_by_id=user.id)
    session.add(ingredient)
    result = await session.execute(select(models.Recipe))
    recipe = result.scalars().first()

    response = await async_client.post(
        f"/recipes/{recipe.id}/ingredients/",
        json={
            **random_ingredient_in_recipe(),
            "ingredient_id": str(ingredient.id),
        },
    )
    assert response.status_code == 401


###############################################################
# # /recipes/{recipe_id: int}/ingredients/{ingredient_id: int}/
###############################################################
@pytest.mark.asyncio
async def test_get_recipe_ingredient_detail(
    dataset: AsyncSession, async_client: AsyncClient
):
    """GET /recipes/{recipe_id: int}/ingredients/{ingredient_id: int}/"""
    session = dataset
    result = await session.execute(
        select(models.Recipe)
        .where(models.Recipe.private.is_(False))
        .options(selectinload(models.Recipe.ingredients))
    )
    recipe = result.scalars().first()
    ingredient = recipe.ingredients[0]
    response = await async_client.get(
        f"/recipes/{recipe.id}/ingredients/{ingredient.ingredient_id}/"
    )
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_edit_recipe_ingredient(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """/recipes/{recipe_id: int}/ingredients/{ingredient_id: int}/"""
    session = dataset
    result = await session.execute(
        select(models.Recipe).options(selectinload(models.Recipe.ingredients))
    )
    recipe = result.scalars().first()
    ingredient = recipe.ingredients[0]
    response = await async_client.put(
        f"/recipes/{recipe.id}/ingredients/{ingredient.ingredient_id}/",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
        json=random_ingredient_in_recipe(),
    )

    assert response.status_code == 200


@pytest.mark.asyncio
async def test_super_user_edit_recipe_ingredient(
    dataset: AsyncSession, async_client: AsyncClient, super_user_token: str
):
    """/recipes/{recipe_id: int}/ingredients/{ingredient_id: int}/"""
    session = dataset
    result = await session.execute(
        select(models.Recipe).options(selectinload(models.Recipe.ingredients))
    )
    recipe = result.scalars().first()
    ingredient = recipe.ingredients[0]
    response = await async_client.put(
        f"/recipes/{recipe.id}/ingredients/{ingredient.ingredient_id}/",
        headers={"Authorization": f"Bearer {super_user_token}"},
        json=random_ingredient_in_recipe(),
    )

    assert response.status_code == 200


@pytest.mark.asyncio
async def test_non_owner_edit_recipe_ingredient(
    dataset: AsyncSession, async_client: AsyncClient, non_owner_user_token: str
):
    """/recipes/{recipe_id: int}/ingredients/{ingredient_id: int}/"""
    session = dataset
    result = await session.execute(
        select(models.Recipe)
        .where(models.Recipe.private.is_(False))
        .options(selectinload(models.Recipe.ingredients))
    )
    recipe = result.scalars().first()
    ingredient = recipe.ingredients[0]
    response = await async_client.put(
        f"/recipes/{recipe.id}/ingredients/{ingredient.ingredient_id}/",
        headers={"Authorization": f"Bearer {non_owner_user_token}"},
        json=random_ingredient_in_recipe(),
    )

    assert response.status_code == 403


@pytest.mark.asyncio
async def test_non_owner_edit_private_recipe_ingredient(
    dataset: AsyncSession, async_client: AsyncClient, non_owner_user_token: str
):
    """/recipes/{recipe_id: int}/ingredients/{ingredient_id: int}/"""
    session = dataset
    result = await session.execute(
        select(models.Recipe)
        .where(models.Recipe.private.is_(True))
        .options(selectinload(models.Recipe.ingredients))
    )
    recipe = result.scalars().first()
    ingredient = recipe.ingredients[0]
    response = await async_client.put(
        f"/recipes/{recipe.id}/ingredients/{ingredient.ingredient_id}/",
        headers={"Authorization": f"Bearer {non_owner_user_token}"},
        json=random_ingredient_in_recipe(),
    )

    assert response.status_code == 404


@pytest.mark.asyncio
async def test_edit_recipe_ingredient_unauthorized(
    dataset: AsyncSession, async_client: AsyncClient
):
    session = dataset
    result = await session.execute(
        select(models.Recipe).options(selectinload(models.Recipe.ingredients))
    )
    recipe = result.scalars().first()
    ingredient = recipe.ingredients[0]
    response = await async_client.put(
        f"/recipes/{recipe.id}/ingredients/{ingredient.ingredient_id}/",
        json=random_ingredient_in_recipe(),
    )

    assert response.status_code == 401


@pytest.mark.asyncio
async def test_delete_recipe_ingredient(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """/recipes/{recipe_id: int}/ingredients/{ingredient_id: int}/"""
    session = dataset
    result = await session.execute(
        select(models.Recipe).options(selectinload(models.Recipe.ingredients))
    )
    recipe = result.scalars().first()
    ingredient = recipe.ingredients[0]
    ingredient_id = ingredient.ingredient_id
    delete_response = await async_client.delete(
        f"/recipes/{recipe.id}/ingredients/{ingredient_id}/",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
    )
    assert delete_response.status_code == 204


@pytest.mark.asyncio
async def test_super_user_delete_recipe_ingredient(
    dataset: AsyncSession, async_client: AsyncClient, super_user_token: str
):
    """/recipes/{recipe_id: int}/ingredients/{ingredient_id: int}/"""
    session = dataset
    result = await session.execute(
        select(models.Recipe).options(selectinload(models.Recipe.ingredients))
    )
    recipe = result.scalars().first()
    ingredient = recipe.ingredients[0]
    ingredient_id = ingredient.ingredient_id
    delete_response = await async_client.delete(
        f"/recipes/{recipe.id}/ingredients/{ingredient_id}/",
        headers={"Authorization": f"Bearer {super_user_token}"},
    )

    assert delete_response.status_code == 204


@pytest.mark.asyncio
async def test_non_owner_delete_recipe_ingredient(
    dataset: AsyncSession, async_client: AsyncClient, non_owner_user_token: str
):
    """/recipes/{recipe_id: int}/ingredients/{ingredient_id: int}/"""
    session = dataset
    result = await session.execute(
        select(models.Recipe)
        .where(models.Recipe.private.is_(False))
        .options(selectinload(models.Recipe.ingredients))
    )
    recipe = result.scalars().first()
    ingredient = recipe.ingredients[0]
    ingredient_id = ingredient.ingredient_id
    delete_response = await async_client.delete(
        f"/recipes/{recipe.id}/ingredients/{ingredient_id}/",
        headers={"Authorization": f"Bearer {non_owner_user_token}"},
    )

    assert delete_response.status_code == 403


@pytest.mark.asyncio
async def test_non_owner_delete_private_recipe_ingredient(
    dataset: AsyncSession, async_client: AsyncClient, non_owner_user_token: str
):
    """/recipes/{recipe_id: int}/ingredients/{ingredient_id: int}/"""
    session = dataset
    result = await session.execute(
        select(models.Recipe)
        .where(models.Recipe.private.is_(True))
        .options(selectinload(models.Recipe.ingredients))
    )
    recipe = result.scalars().first()
    ingredient = recipe.ingredients[0]
    ingredient_id = ingredient.ingredient_id
    delete_response = await async_client.delete(
        f"/recipes/{recipe.id}/ingredients/{ingredient_id}/",
        headers={"Authorization": f"Bearer {non_owner_user_token}"},
    )

    assert delete_response.status_code == 404


@pytest.mark.asyncio
async def test_delete_recipe_ingredient_unauthorized(
    dataset: AsyncSession, async_client: AsyncClient
):
    session = dataset
    result = await session.execute(
        select(models.Recipe).options(selectinload(models.Recipe.ingredients))
    )
    recipe = result.scalars().first()
    ingredient = recipe.ingredients[0]

    delete_response = await async_client.delete(
        f"/recipes/{recipe.id}/ingredients/{ingredient.ingredient_id}/",
    )
    assert delete_response.status_code == 401


############################
# # /{recipe_id: int}/steps/
############################
@pytest.mark.asyncio
async def test_get_recipe_steps(dataset: AsyncSession, async_client: AsyncClient):
    """GET recipes/{recipe_id: int}/steps/"""
    session = dataset
    result = await session.execute(
        select(models.Recipe).where(models.Recipe.private.is_(False))
    )
    recipe = result.scalars().first()
    response = await async_client.get(
        f"/recipes/{recipe.id}/steps/", params={"page_size": DEFAULT_PAGE_SIZE}
    )
    result = await session.execute(
        select(func.count("*"))
        .select_from(models.Step)
        .where(models.Step.recipe_id == recipe.id)
    )
    response_json = response.json()
    assert (
        len(response_json["items"]) <= DEFAULT_PAGE_SIZE
        and DEFAULT_PAGE_SIZE == response_json["pageSize"]
        and response_json["totalItems"] == result.scalar()
    )


@pytest.mark.asyncio
async def test_create_recipe_steps(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """GET recipes/{recipe_id: int}/steps/"""
    session = dataset
    result = await session.execute(select(models.Recipe))
    recipe = result.scalars().first()
    response = await async_client.post(
        f"/recipes/{recipe.id}/steps/",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
        json=random_step(),
    )

    result = await session.execute(
        select(func.count("*"))
        .select_from(models.Step)
        .where(models.Step.recipe_id == recipe.id)
    )

    assert response.status_code == 201 and response.json()["order"] == result.scalar()


@pytest.mark.asyncio
async def test_super_user_create_recipe_steps(
    dataset: AsyncSession, async_client: AsyncClient, super_user_token: str
):
    """GET recipes/{recipe_id: int}/steps/"""
    session = dataset
    result = await session.execute(select(models.Recipe))
    recipe = result.scalars().first()
    response = await async_client.post(
        f"/recipes/{recipe.id}/steps/",
        headers={"Authorization": f"Bearer {super_user_token}"},
        json=random_step(),
    )

    result = await session.execute(
        select(func.count("*"))
        .select_from(models.Step)
        .where(models.Step.recipe_id == recipe.id)
    )

    assert response.status_code == 201 and response.json()["order"] == result.scalar()


@pytest.mark.asyncio
async def test_non_owner_create_recipe_steps(
    dataset: AsyncSession, async_client: AsyncClient, non_owner_user_token
):
    session = dataset
    result = await session.execute(
        select(models.Recipe).where(models.Recipe.private.is_(False))
    )
    recipe = result.scalars().first()
    response = await async_client.post(
        f"/recipes/{recipe.id}/steps/",
        headers={"Authorization": f"Bearer {non_owner_user_token}"},
        json=random_step(),
    )
    assert response.status_code == 403


@pytest.mark.asyncio
async def test_non_owner_create_private_recipe_steps(
    dataset: AsyncSession, async_client: AsyncClient, non_owner_user_token
):
    session = dataset
    result = await session.execute(
        select(models.Recipe).where(models.Recipe.private.is_(True))
    )
    recipe = result.scalars().first()
    response = await async_client.post(
        f"/recipes/{recipe.id}/steps/",
        headers={"Authorization": f"Bearer {non_owner_user_token}"},
        json=random_step(),
    )
    assert response.status_code == 404


@pytest.mark.asyncio
async def test_create_recipe_steps_unauthorized(
    dataset: AsyncSession, async_client: AsyncClient
):
    session = dataset
    result = await session.execute(select(models.Recipe))
    recipe = result.scalars().first()
    response = await async_client.post(
        f"/recipes/{recipe.id}/steps/",
        json=random_step(),
    )
    assert response.status_code == 401


########################################
# # /{recipe_id: int}/steps/{order: int}
########################################
@pytest.mark.asyncio
async def test_get_recipe_step_detail(dataset: AsyncSession, async_client: AsyncClient):
    """GET recipes/{recipe_id: int}/steps/{order: int}"""
    session = dataset
    result = await session.execute(
        select(models.Recipe)
        .where(models.Recipe.private.is_(False))
        .options(selectinload(models.Recipe.steps))
    )
    recipe = result.scalars().first()
    step = recipe.steps[0]

    response = await async_client.get(f"/recipes/{recipe.id}/steps/{step.order}")
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_edit_recipe_step(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """PUT recipes/{recipe_id: int}/steps/{order: int}"""
    session = dataset
    result = await session.execute(
        select(models.Recipe).options(selectinload(models.Recipe.steps))
    )
    recipe = result.scalars().first()
    step = recipe.steps[0]
    response = await async_client.put(
        f"/recipes/{recipe.id}/steps/{step.order}/",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
        json=random_step(),
    )
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_edit_recipe_step_non_owner(
    dataset: AsyncSession, async_client: AsyncClient, non_owner_user_token: str
):
    """PUT recipes/{recipe_id: int}/steps/{order: int}"""
    session = dataset
    result = await session.execute(
        select(models.Recipe)
        .where(models.Recipe.private.is_(False))
        .options(selectinload(models.Recipe.steps))
    )
    recipe = result.scalars().first()
    step = recipe.steps[0]
    response = await async_client.put(
        f"/recipes/{recipe.id}/steps/{step.order}/",
        headers={"Authorization": f"Bearer {non_owner_user_token}"},
        json=random_step(),
    )
    assert response.status_code == 403


@pytest.mark.asyncio
async def test_edit_private_recipe_step_non_owner(
    dataset: AsyncSession, async_client: AsyncClient, non_owner_user_token: str
):
    """PUT recipes/{recipe_id: int}/steps/{order: int}"""
    session = dataset
    result = await session.execute(
        select(models.Recipe)
        .where(models.Recipe.private.is_(True))
        .options(selectinload(models.Recipe.steps))
    )
    recipe = result.scalars().first()
    step = recipe.steps[0]
    response = await async_client.put(
        f"/recipes/{recipe.id}/steps/{step.order}/",
        headers={"Authorization": f"Bearer {non_owner_user_token}"},
        json=random_step(),
    )
    assert response.status_code == 404


@pytest.mark.asyncio
async def test_edit_recipe_step_unauthorized(
    dataset: AsyncSession, async_client: AsyncClient
):
    session = dataset
    result = await session.execute(
        select(models.Recipe).options(selectinload(models.Recipe.steps))
    )
    recipe = result.scalars().first()
    step = recipe.steps[0]
    response = await async_client.put(
        f"/recipes/{recipe.id}/steps/{step.order}/",
        json=random_step(),
    )
    assert response.status_code == 401


@pytest.mark.asyncio
async def test_recipe_step_delete(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    session = dataset
    result = await session.execute(
        select(models.Recipe).options(selectinload(models.Recipe.steps))
    )
    recipe = result.scalars().first()
    step = recipe.steps[-1]
    response = await async_client.delete(
        f"/recipes/{recipe.id}/steps/{step.order}/",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
    )
    assert response.status_code == 204


@pytest.mark.asyncio
async def test_recipe_step_delete_non_owner(
    dataset: AsyncSession, async_client: AsyncClient, non_owner_user_token: str
):
    session = dataset
    result = await session.execute(
        select(models.Recipe)
        .where(models.Recipe.private.is_(False))
        .options(selectinload(models.Recipe.steps))
    )
    recipe = result.scalars().first()
    step = recipe.steps[-1]
    response = await async_client.delete(
        f"/recipes/{recipe.id}/steps/{step.order}/",
        headers={"Authorization": f"Bearer {non_owner_user_token}"},
    )
    assert response.status_code == 403


@pytest.mark.asyncio
async def test_recipe_step_delete_unauthorized(
    dataset: AsyncSession, async_client: AsyncClient
):
    session = dataset
    result = await session.execute(
        select(models.Recipe).options(selectinload(models.Recipe.steps))
    )
    recipe = result.scalars().first()
    step = recipe.steps[-1]
    response = await async_client.delete(
        f"/recipes/{recipe.id}/steps/{step.order}/",
    )
    assert response.status_code == 401


@pytest.mark.asyncio
async def test_recipe_step_delete_not_last_step(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    session = dataset
    result = await session.execute(
        select(models.Recipe).options(selectinload(models.Recipe.steps))
    )
    recipe = result.scalars().first()
    step = recipe.steps[0]
    response = await async_client.delete(
        f"/recipes/{recipe.id}/steps/{step.order}/",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
    )
    assert response.status_code == 409


##########################
# # /{recipe_id: int}/tags
##########################
@pytest.mark.asyncio
async def test_get_recipe_tags(dataset: AsyncSession, async_client: AsyncClient):
    """GET recipes/{recipe_id: int}/tags/"""
    session = dataset
    result = await session.execute(
        select(models.Recipe)
        .where(models.Recipe.private.is_(False))
        .options(selectinload(models.Recipe.tags))
    )
    recipe = result.scalars().first()
    response = await async_client.get(f"recipes/{recipe.id}/tags/")
    response_json = response.json()
    result = await session.execute(
        select(func.count("*"))
        .select_from(models.recipe_tags)
        .where(models.recipe_tags.c["recipe_id"] == recipe.id)
    )
    assert (
        len(response_json["items"]) <= DEFAULT_PAGE_SIZE
        and DEFAULT_PAGE_SIZE == response_json["pageSize"]
        and response_json["totalItems"] == result.scalar()
    )


@pytest.mark.asyncio
async def test_add_recipe_tags(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """POST recipes/{recipe_id: int}/tags/"""
    session = dataset
    result = await session.execute(select(models.User))
    user = result.scalars().first()
    tag = models.Tag(**random_tag(), created_by_id=user.id)
    session.add(tag)
    result = await session.execute(select(models.Recipe))
    recipe = result.scalars().first()
    await session.commit()
    response = await async_client.post(
        f"/recipes/{recipe.id}/tags/",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
        json={"id": str(tag.id)},
    )
    assert response.status_code == 201


@pytest.mark.asyncio
async def test_add_recipe_tags_unauthorized(
    dataset: AsyncSession, async_client: AsyncClient
):
    session = dataset
    result = await session.execute(select(models.User))
    user = result.scalars().first()
    tag = models.Tag(**random_tag(), created_by_id=user.id)
    session.add(tag)
    result = await session.execute(select(models.Recipe))
    recipe = result.scalars().first()
    response = await async_client.post(
        f"/recipes/{recipe.id}/tags/",
        json={"id": str(tag.id)},
    )
    assert response.status_code == 401


########################################
# # /{recipe_id: int}/tags/{tag_id: int}
########################################
@pytest.mark.asyncio
async def test_remove_recipe_tag(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """DELETE recipes/{recipe_id: int}/tags/"""
    session = dataset
    result = await session.execute(
        select(models.Recipe).options(selectinload(models.Recipe.tags))
    )
    recipe = result.scalars().first()
    tag = recipe.tags[0]
    tag_id = tag.id
    response = await async_client.delete(
        f"recipes/{recipe.id}/tags/{tag_id}/",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
    )
    print(response.json())
    result = await session.execute(select(models.Tag).where(models.Tag.id == tag_id))
    tag = result.scalar()
    assert response.status_code == 204 and tag is not None


@pytest.mark.asyncio
async def test_remove_recipe_tag_unauthorzed(
    dataset: AsyncSession, async_client: AsyncClient
):
    session = dataset
    result = await session.execute(
        select(models.Recipe).options(selectinload(models.Recipe.tags))
    )
    recipe = result.scalars().first()
    tag = recipe.tags[0]
    response = await async_client.delete(
        f"recipes/{recipe.id}/tags/{tag.id}",
    )
    assert response.status_code == 401


################################
# # Entire create recipe process
################################


@pytest.mark.asyncio
async def test_create_recipe_with_ingredients_and_steps(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """Test creating ingredients, creating recipe, adding ingredients,
    steps and tags to recipe"""
    ingredient_responses = [
        await async_client.post(
            "/ingredients/",
            headers={"Authorization": f"Bearer {logged_in_user_token}"},
            json=random_ingredient(),
        )
        for _ in range(3)
    ]

    for response in ingredient_responses:
        assert response.status_code == 201

    recipe_response = await async_client.post(
        "/recipes/",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
        json=random_recipe(),
    )

    assert recipe_response.status_code == 201
    recipe = recipe_response.json()

    ingredient_in_recipe_responses = [
        await async_client.post(
            f"/recipes/{recipe['id']}/ingredients/",
            headers={"Authorization": f"Bearer {logged_in_user_token}"},
            json={
                **random_ingredient_in_recipe(),
                "ingredient_id": ingredient.json()["id"],
            },
        )
        for ingredient in ingredient_responses
    ]

    for response in ingredient_in_recipe_responses:
        assert response.status_code == 201
