"""
Test for /ingredients/
"""
import pytest
from httpx import AsyncClient
from sqlalchemy import func, select
from sqlalchemy.ext.asyncio import AsyncSession

from app.constants import DEFAULT_PAGE_SIZE
from app.database import models
from tests import fake, random_ingredient

###############
# /ingredients/
###############


@pytest.mark.asyncio
async def test_get_ingredient_list_default_page_size(
    dataset: AsyncSession, async_client: AsyncClient
):
    """GET /ingredients/"""
    session = dataset
    response = await async_client.get(
        "/ingredients/", params={"page_size": DEFAULT_PAGE_SIZE}
    )
    assert response.status_code == 200
    result = await session.execute(
        select(func.count("*")).select_from(models.Ingredient)
    )
    response_json = response.json()
    assert (
        len(response_json["items"]) <= DEFAULT_PAGE_SIZE
        and DEFAULT_PAGE_SIZE == response_json["pageSize"]
        and response_json["totalItems"] == result.scalar()
    )


@pytest.mark.asyncio
async def test_get_ingredient_list_diffrent_page_size(
    dataset: AsyncSession, async_client: AsyncClient
):
    """GET /ingredients/"""
    session = dataset
    page_size = fake.pyint()
    response = await async_client.get("/ingredients/", params={"page_size": page_size})
    assert response.status_code == 200
    result = await session.execute(
        select(func.count("*")).select_from(models.Ingredient)
    )
    response_json = response.json()
    assert (
        len(response_json["items"]) <= page_size
        and page_size == response_json["pageSize"]
        and response_json["totalItems"] == result.scalar()
    )


@pytest.mark.asyncio
async def test_get_ingredient_list_all_pages(
    dataset: AsyncSession, async_client: AsyncClient
):
    """GET /ingredients/"""
    session = dataset
    response = await async_client.get(
        "/ingredients/", params={"page_size": DEFAULT_PAGE_SIZE}
    )
    assert response.status_code == 200
    response_json = response.json()
    result = await session.execute(
        select(func.count("*")).select_from(models.Ingredient)
    )
    total_items_db = result.scalar()
    total_items = response_json["totalItems"]
    assert total_items_db == total_items

    items_seen = len(response_json["items"])
    while len(response_json["items"]) == DEFAULT_PAGE_SIZE:
        prev_id = response_json["items"][-1]["id"]
        response = await async_client.get(
            "/ingredients/",
            params={"page_size": DEFAULT_PAGE_SIZE, "prev_id": prev_id},
        )
        response_json = response.json()
        items_seen += len(response_json["items"])

    assert items_seen == total_items


@pytest.mark.asyncio
async def test_post_ingredient(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """POST /ingredients/"""
    session = dataset
    response = await async_client.post(
        "/ingredients/",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
        json=random_ingredient(),
    )

    assert response.status_code == 201
    data = response.json()
    result = await session.execute(
        select(models.Ingredient).where(models.Ingredient.id == data["id"])
    )
    assert result.scalar() is not None


@pytest.mark.asyncio
async def test_post_ingredient_unauthorized(
    dataset: AsyncSession,
    async_client: AsyncClient,
):
    response = await async_client.post(
        "/ingredients/",
        json=random_ingredient(),
    )
    assert response.status_code == 401


@pytest.mark.asyncio
async def test_post_ingredient_already_exists(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """POST /ingredients/ already exits"""
    session = dataset
    result = await session.execute(select(models.Ingredient))
    ingredient = result.scalars().first()
    response = await async_client.post(
        "/ingredients/",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
        json={"name": ingredient.name},
    )

    assert response.status_code == 409


#########################
# /ingredients/{id: int}/
#########################


@pytest.mark.asyncio
async def test_get_ingredient_detail(dataset: AsyncSession, async_client: AsyncClient):
    """GET /ingredient/pk/"""
    session = dataset
    result = await session.execute(select(models.Ingredient))
    ingredient = result.scalars().first()
    response = await async_client.get(f"/ingredients/{ingredient.id}/")
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_edit_ingredient_owner(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """PUT /ingredient/pk/"""
    session = dataset
    result = await session.execute(select(models.Ingredient))
    ingredient = result.scalars().first()
    ingredient_id = ingredient.id
    changed_ingredient = random_ingredient()
    response = await async_client.put(
        f"/ingredients/{ingredient_id}",
        json=changed_ingredient,
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
    )
    result = await session.execute(
        select(models.Ingredient).where(models.Ingredient.id == ingredient_id)
    )
    ingredient = result.scalars().first()
    assert response.status_code == 200 and changed_ingredient["name"] == ingredient.name


@pytest.mark.asyncio
async def test_edit_ingredient_nonowner(
    dataset: AsyncSession, async_client: AsyncClient, non_owner_user_token: str
):
    """PUT /ingredient/pk/"""
    session = dataset
    result = await session.execute(select(models.Ingredient))
    ingredient = result.scalars().first()
    response = await async_client.put(
        f"/ingredients/{ingredient.id}",
        json=random_ingredient(),
        headers={"Authorization": f"Bearer {non_owner_user_token}"},
    )
    assert response.status_code == 403


@pytest.mark.asyncio
async def test_edit_ingredient_superuser(
    dataset: AsyncSession, async_client: AsyncClient, super_user_token: str
):
    """PUT /ingredient/pk/"""
    session = dataset
    result = await session.execute(select(models.Ingredient))
    ingredient = result.scalars().first()
    ingredient_id = ingredient.id
    changed_ingredient = random_ingredient()
    response = await async_client.put(
        f"/ingredients/{ingredient_id}",
        json=changed_ingredient,
        headers={"Authorization": f"Bearer {super_user_token}"},
    )
    result = await session.execute(
        select(models.Ingredient).where(models.Ingredient.id == ingredient_id)
    )
    ingredient = result.scalars().first()
    assert response.status_code == 200 and ingredient.name == changed_ingredient["name"]


@pytest.mark.asyncio
async def test_delete_ingredient_owner(
    dataset: AsyncSession, async_client: AsyncClient, logged_in_user_token: str
):
    """DELETE /ingredient/pk/"""
    session = dataset
    result = await session.execute(select(models.Ingredient))
    ingredient = result.scalars().first()
    ingredient_id = ingredient.id
    response = await async_client.delete(
        f"/ingredients/{ingredient.id}",
        headers={"Authorization": f"Bearer {logged_in_user_token}"},
    )
    result = await session.execute(
        select(models.Ingredient).where(models.Ingredient.id == ingredient_id)
    )
    ingredient = result.scalars().first()
    assert response.status_code == 204 and ingredient is None


@pytest.mark.asyncio
async def test_delete_ingredient_nonowner(
    dataset: AsyncSession, async_client: AsyncClient, non_owner_user_token: str
):
    """DELETE /ingredient/pk/"""
    session = dataset
    result = await session.execute(select(models.Ingredient))
    ingredient = result.scalars().first()
    ingredient_id = ingredient.id
    response = await async_client.delete(
        f"/ingredients/{ingredient.id}",
        headers={"Authorization": f"Bearer {non_owner_user_token}"},
    )
    result = await session.execute(
        select(models.Ingredient).where(models.Ingredient.id == ingredient_id)
    )
    ingredient = result.scalars().first()
    assert response.status_code == 403 and ingredient is not None


@pytest.mark.asyncio
async def test_delete_ingredient_superuser(
    dataset: AsyncSession, async_client: AsyncClient, super_user_token: str
):
    """DELETE /ingredient/pk/"""
    session = dataset
    result = await session.execute(select(models.Ingredient))
    ingredient = result.scalars().first()
    ingredient_id = ingredient.id
    response = await async_client.delete(
        f"/ingredients/{ingredient.id}",
        headers={"Authorization": f"Bearer {super_user_token}"},
    )
    result = await session.execute(
        select(models.Ingredient).where(models.Ingredient.id == ingredient_id)
    )
    ingredient = result.scalars().first()
    assert response.status_code == 204 and ingredient is None


@pytest.mark.asyncio
async def test_delete_ingredient_used_in_recipe(
    dataset: AsyncSession, async_client: AsyncClient, super_user_token: str
):
    """DELETE /ingredient/pk/"""
    session = dataset
    result = await session.execute(select(models.IngredientInRecipe))
    ingredient_in_recipe = result.scalars().first()
    ingredient_id = ingredient_in_recipe.ingredient_id
    response = await async_client.delete(
        f"/ingredients/{ingredient_id}",
        headers={"Authorization": f"Bearer {super_user_token}"},
    )
    result = await session.execute(
        select(models.Ingredient).where(models.Ingredient.id == ingredient_id)
    )
    ingredient = result.scalars().first()
    assert response.status_code == 409 and ingredient is not None
