import operator
from collections import defaultdict

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.middleware.gzip import GZipMiddleware

from app.config import ORGINS
from app.routers import ingredients, meal_plans, recipes, tags, tasks, token


def app_factory() -> FastAPI:
    """Create fastAPI app with routers included

    Returns:
        FastAPI: app
    """
    app = FastAPI()

    app.add_middleware(GZipMiddleware, minimum_size=1000)
    app.add_middleware(
        CORSMiddleware,
        allow_origins=ORGINS,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    app.include_router(ingredients.router)
    app.include_router(tags.router)
    app.include_router(recipes.router)
    app.include_router(meal_plans.router)
    app.include_router(token.router)
    app.include_router(tasks.router)

    return app


app = app_factory()


@app.get("/", tags=["util"])
def get_all_routes():
    """Show all available routes"""
    routes = defaultdict(set)
    for route in app.routes:
        routes[route.path].update(route.methods)

    return sorted(
        (
            {"path": path, "allowed_methods": sorted(methods)}
            for path, methods in routes.items()
        ),
        key=operator.itemgetter("path"),
    )
