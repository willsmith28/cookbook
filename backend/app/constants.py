"""Constants"""
from enum import Enum


class TagTypes(str, Enum):
    cuisine = "Cuisine"
    meal = "Meal"
    prep_method = "Prep Method"


class IngredientUnits(str, Enum):
    teaspoon = "teaspoon"
    tablespoon = "tablespoon"
    fluid_ounce = "fluid ounce"
    cup = "cup"
    pint = "pint"
    quart = "quart"
    gallon = "gallon"
    milliliter = "milliliter"
    liter = "liter"
    pound = "pound"
    ounce = "ounce"
    gram = "gram"
    inch = "inch"
    millimeter = "millimeter"
    centimeter = "centimeter"
    pieces = "pieces"


class MealPlanTypes(str, Enum):
    breakfast = "Breakfast"
    lunch = "Lunch"
    dinner = "Dinner"
    snack = "Snack"


DETAIL_404 = "The resource you requested could not be found."
DETAIL_403 = "You are not allowed to edit resources you did not create."

DEFAULT_PAGE_SIZE = 100
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 15
REFRESH_TOKEN_EXPIRE_MINUTES = 60 * 24 * 7
ISSUER = "wills_cookbook"
