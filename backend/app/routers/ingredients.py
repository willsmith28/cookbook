import asyncio
import csv
import io
import uuid
from collections.abc import Iterable
from typing import Optional

from fastapi import APIRouter, Depends, HTTPException, Response, status
from sqlalchemy import delete, func, select, update
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession

from app import serializers
from app.auth import get_current_active_user
from app.constants import DEFAULT_PAGE_SIZE, IngredientUnits
from app.database import get_database_session, models
from app.utils import get_instance_or_404

router = APIRouter(
    prefix="/ingredients",
    tags=["ingredients"],
    responses={404: {"description": "Not found"}},
)


##############
# /ingredients
##############
@router.get("/", response_model=serializers.PaginatedIngredient)
async def get_ingredients(
    session: AsyncSession = Depends(get_database_session),
    page_size: int = DEFAULT_PAGE_SIZE,
    prev_id: Optional[uuid.UUID] = None,
):
    """Get all ingredients"""
    ingredient_query = models.Ingredient.paginated_id_query(
        prev_id=prev_id, page_size=page_size
    )
    total_items_query = select(func.count("*")).select_from(models.Ingredient)
    ingredients_result, total_count_result = await asyncio.gather(
        session.execute(ingredient_query), session.execute(total_items_query)
    )

    return {
        "items": ingredients_result.scalars().all(),
        "total_items": total_count_result.scalar(),
        "page_size": page_size,
        "prev_id": prev_id,
    }


@router.post(
    "/",
    response_model=serializers.Ingredient,
    responses={409: {"description": "Post caused conflict"}},
    status_code=201,
)
async def create_ingredient(
    ingredient: serializers.IngredientBase,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    """create new Ingredient"""
    new_ingredient = models.Ingredient(
        **ingredient.dict(), created_by_id=current_user.id
    )
    try:
        session.add(new_ingredient)
        await session.commit()

    except IntegrityError as error:
        await session.rollback()
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail=str(error)
        ) from error

    return new_ingredient


####################
# /ingredients/units
####################
@router.get("/units/", response_model=Iterable[str])
def get_ingredient_units():
    """get list of ingredient units to be used in select"""
    return tuple(IngredientUnits)


#########################
# /ingredients/tag-phrase
#########################
@router.get("/tag-phrase/")
async def create_ingredient_phrase_tagging_csv(
    json: bool = False,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    """Download training csv for ingredient phrase tagging"""
    if not current_user.super_user:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You do not have permissions to create the training csv",
        )

    result = await session.execute(select(models.IngredientPhraseTaggingHistory))

    if json:
        return result.scalars().all()

    with io.StringIO(newline="") as csv_buffer:
        csv_writer = csv.writer(csv_buffer)
        csv_writer.writerow(
            (
                "index",
                "input",
                "name",
                "qty",
                "range_end",
                "unit",
                "comment",
            )
        )
        csv_writer.writerows(
            (
                index,
                item.input,
                item.name,
                item.qty,
                item.range_end,
                item.unit,
                item.comment,
            )
            for index, item in enumerate(result.scalars().all())
        )
        csv_buffer.seek(0)
        return Response(csv_buffer.read(), media_type="text/csv")


@router.post("/tag-phrase/", response_model=serializers.IngredientPhraseTaggingHistory)
async def tag_ingredient_phrase(
    ingredient_phrase: serializers.IngredientPhraseTaggingHistoryBase,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    """create new tagged ingredient phrase"""
    if not current_user.super_user:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You do not have permissions to create tagged ingredient phrases",
        )

    new_ingredient_phrase = models.IngredientPhraseTaggingHistory(
        **ingredient_phrase.dict()
    )
    try:
        session.add(new_ingredient_phrase)
        await session.commit()

    except IntegrityError as error:
        await session.rollback()
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail=str(error)
        ) from error

    return new_ingredient_phrase


@router.get("/tag-phrase/{ingredient_phrase_id}/")
async def get_ingredient_phrase_detail(
    ingredient_phrase_id: uuid.UUID,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    if not current_user.super_user:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You do not have permissions to view tagged ingredient phrases",
        )

    return await get_instance_or_404(
        session, models.IngredientPhraseTaggingHistory, id=ingredient_phrase_id
    )


@router.put("/tag-phrase/{ingredient_phrase_id}/")
async def edit_ingredient_phrase(
    ingredient_phrase_id: uuid.UUID,
    tagged_phrase: serializers.IngredientPhraseTaggingHistoryBase,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    """Edit tagged ingredient phrase"""
    if not current_user.super_user:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You do not have permissions to edit tagged ingredient phrases",
        )

    try:
        await session.execute(
            update(models.IngredientPhraseTaggingHistory)
            .where(models.IngredientPhraseTaggingHistory.id == ingredient_phrase_id)
            .values(**tagged_phrase.dict())
        )

    except IntegrityError as error:
        await session.rollback()
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail=str(error)
        ) from error


###################################
# /ingredients/{ingredient_id: uuid.UUID}
###################################
@router.get(
    "/{ingredient_id}/",
    response_model=serializers.Ingredient,
)
async def get_ingredient_detail(
    ingredient_id: uuid.UUID,
    session: AsyncSession = Depends(get_database_session),
):
    """Get ingredient detail"""
    return await get_instance_or_404(session, models.Ingredient, id=ingredient_id)


@router.put(
    "/{ingredient_id}/",
    response_model=serializers.Ingredient,
    responses={
        403: {"description": "Not allowed"},
        409: {"description": "Put caused conflict"},
    },
)
async def edit_ingredient(
    ingredient_id: uuid.UUID,
    ingredient: serializers.IngredientBase,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    """Edit ingredient"""
    db_ingredient = await get_instance_or_404(
        session, models.Ingredient, id=ingredient_id
    )

    if not current_user.super_user and db_ingredient.created_by_id != current_user.id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You are not allowed to edit ingredients you did not create",
        )

    try:
        await session.execute(
            update(models.Ingredient)
            .where(models.Ingredient.id == ingredient_id)
            .values(**ingredient.dict())
        )

    except IntegrityError as error:
        await session.rollback()
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail=str(error)
        ) from error

    return db_ingredient


@router.delete(
    "/{ingredient_id}/",
    responses={
        403: {"description": "Not allowed"},
        409: {"description": "Delete caused conflict"},
    },
    response_class=Response,
    status_code=status.HTTP_204_NO_CONTENT,
)
async def delete_ingredient(
    ingredient_id: uuid.UUID,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    """Delete ingredient"""
    db_ingredient = await get_instance_or_404(
        session, models.Ingredient, id=ingredient_id
    )

    if not current_user.super_user and db_ingredient.created_by_id != current_user.id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You are not allowed to delete ingredients you did not create",
        )

    try:
        await session.execute(
            delete(models.Ingredient).where(models.Ingredient.id == ingredient_id)
        )

    except IntegrityError as error:
        await session.rollback()
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail=str(error)
        ) from error
