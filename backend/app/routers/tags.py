import asyncio
import uuid
from typing import Optional

from fastapi import APIRouter, Depends, HTTPException, Response, status
from sqlalchemy import delete, func, select, update
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession

from app import serializers
from app.auth import get_current_active_user
from app.constants import DEFAULT_PAGE_SIZE, TagTypes
from app.database import get_database_session, models
from app.utils import get_instance_or_404

router = APIRouter(
    prefix="/tags", tags=["tags"], responses={404: {"description": "Not found"}}
)


#######
# /tags
#######
@router.get("/", response_model=serializers.PaginatedTag)
async def get_tags(
    session: AsyncSession = Depends(get_database_session),
    page_size: int = DEFAULT_PAGE_SIZE,
    prev_id: Optional[uuid.UUID] = None,
):
    """Get list of tags"""
    tags_query = models.Tag.paginated_id_query(prev_id=prev_id, page_size=page_size)
    total_items_query = select(func.count("*")).select_from(models.Tag)

    tags_result, total_count_result = await asyncio.gather(
        session.execute(tags_query), session.execute(total_items_query)
    )
    return {
        "items": tags_result.scalars().all(),
        "total_items": total_count_result.scalar(),
        "page_size": page_size,
        "prev_id": prev_id,
    }


@router.post(
    "/",
    response_model=serializers.Tag,
    responses={409: {"description": "Post caused conflict"}},
    status_code=201,
)
async def create_tag(
    tag: serializers.TagBase,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    """Add new tag"""
    new_tag = models.Tag(**tag.dict(), created_by_id=current_user.id)
    try:
        session.add(new_tag)
        await session.commit()

    except IntegrityError as error:
        await session.rollback()
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail=str(error)
        ) from error

    return new_tag


#############
# /tags/types
#############
@router.get("/types/", response_model=list[str])
def tag_types():
    """Get list of tag types to use in select"""
    return tuple(TagTypes)


#####################
# /tags/{tag_id: uuid.UUID}
#####################
@router.get(
    "/{tag_id}/",
    response_model=serializers.Tag,
)
async def get_tag_detail(
    tag_id: uuid.UUID, session: AsyncSession = Depends(get_database_session)
):
    """Get tag detail"""
    return await get_instance_or_404(session, models.Tag, id=tag_id)


@router.put(
    "/{tag_id}/",
    responses={
        409: {"description": "Put caused conflict"},
        403: {"description": "Not allowed"},
    },
    response_model=serializers.Tag,
)
async def edit_tag(
    tag_id: uuid.UUID,
    tag: serializers.TagBase,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    """Edit tag"""
    db_tag = await get_instance_or_404(session, models.Tag, id=tag_id)

    if not current_user.super_user and db_tag.created_by_id != current_user.id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You are not allowed to edit tags you did not create",
        )

    try:
        await session.execute(
            update(models.Tag).where(models.Tag.id == tag_id).values(**tag.dict())
        )

    except IntegrityError as error:
        await session.rollback()
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail=str(error)
        ) from error

    return db_tag


@router.delete(
    "/{tag_id}/",
    responses={
        409: {"description": "Put caused conflict"},
        403: {"description": "Not allowed"},
    },
    response_class=Response,
    status_code=status.HTTP_204_NO_CONTENT,
)
async def delete_tag(
    tag_id: uuid.UUID,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    """Delete tag"""
    db_tag = await get_instance_or_404(session, models.Tag, id=tag_id)

    if not current_user.super_user and db_tag.created_by_id != current_user.id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You are not allowed to delete tags you did not create",
        )

    try:
        await session.execute(delete(models.Tag).where(models.Tag.id == tag_id))

    except IntegrityError as error:
        await session.rollback()
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail=str(error)
        ) from error
