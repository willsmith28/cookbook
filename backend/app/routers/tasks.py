from fastapi import APIRouter

from app.celery import Tasks
from app.celery import app as celery_app
from app.serializers import CustomBaseModel

router = APIRouter(
    prefix="/tasks",
    tags=["tasks"],
    responses={404: {"description": "Not found"}},
)


@router.get("/add", response_model=dict[str, int])
def add(
    x: int,
    y: int,
):
    return {"result": celery_app.send_task(Tasks.add.value, args=(x, y)).get()}


class Ingredients(CustomBaseModel):
    ingredients: list[str]


@router.post("/tag-ingredients")
def tag_ingredients(ingredients: Ingredients):
    return {
        "result": celery_app.send_task(
            Tasks.tag_ingredient_phrase.value, args=(ingredients.ingredients,)
        ).get()
    }
