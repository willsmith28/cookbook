import datetime
from typing import Optional

from fastapi import APIRouter, Cookie, Depends, HTTPException, Response, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy import update
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession

from app import serializers
from app.auth import (
    RefreshToken,
    RefreshTokenResponse,
    Token,
    authenticate_access_token,
    authenticate_user,
    create_token,
    get_password_hash,
)
from app.constants import ACCESS_TOKEN_EXPIRE_MINUTES, REFRESH_TOKEN_EXPIRE_MINUTES
from app.database import get_database_session, models

router = APIRouter(prefix="/token", tags=["token"])


@router.post("/", response_model=Token)
async def login(
    response: Response,
    form_data: OAuth2PasswordRequestForm = Depends(),
    session: AsyncSession = Depends(get_database_session),
) -> Token:
    """Get user token"""
    user = await authenticate_user(session, form_data.username, form_data.password)
    access_token = create_token(
        user=user, expires_delta=datetime.timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    )
    refresh_token = create_token(
        user=user,
        expires_delta=datetime.timedelta(minutes=REFRESH_TOKEN_EXPIRE_MINUTES),
    )

    await session.execute(
        update(models.User)
        .where(models.User.id == user.id)
        .values(refresh_token=refresh_token)
    )

    set_cookies(response, access_token=access_token, refresh_token=refresh_token)

    return {
        "access_token": access_token,
        "refresh_token": refresh_token,
        "token_type": "bearer",
    }


@router.post("/refresh/", response_model=RefreshTokenResponse)
async def refresh(
    response: Response,
    refresh: Optional[RefreshToken] = None,
    refresh_cookie: Optional[str] = Cookie(None, alias="refresh"),
    session: AsyncSession = Depends(get_database_session),
):
    if refresh is None and refresh_cookie is None:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail={
                "loc": ["body"],
                "msg": "refresh token must be provided in body or cookie",
                "type": "value_error.missing",
            },
        )

    refresh_token = refresh.refresh if refresh else refresh_cookie
    user = await authenticate_access_token(refresh_token, session)
    if user is None or user.refresh_token != refresh_token:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Refresh token could not be authenticated",
        )

    access_token = create_token(
        user=user, expires_delta=datetime.timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    )

    set_cookies(response, access_token=access_token)

    return {
        "access_token": access_token,
        "token_type": "bearer",
    }


@router.post(
    "/sign-up/",
    responses={409: {"description": "Post caused conflict"}},
    status_code=status.HTTP_201_CREATED,
)
async def user_sign_up(
    new_user: serializers.UserSignUp,
    session: AsyncSession = Depends(get_database_session),
) -> Token:
    """sign up as a new user"""
    new_user_dict = new_user.dict()
    new_user_dict.update(
        {"hashed_password": get_password_hash(new_user_dict.pop("password"))}
    )
    new_user = models.User(**new_user_dict)
    try:
        session.add(new_user)
        await session.commit()

    except IntegrityError as error:
        await session.rollback()
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Username or email already exits",
        ) from error

    return {"message": "You have succesfully signed up. Please log in to continue."}


@router.post("/logout", response_class=Response, status_code=status.HTTP_204_NO_CONTENT)
def logout(response: Response):
    """Removes tokens from cookies"""
    response.delete_cookie("token")
    response.delete_cookie("refresh")


def set_cookies(
    response: Response,
    access_token: Optional[str] = None,
    refresh_token: Optional[str] = None,
):
    """sets access and refresh tokens as cookies if provided

    Args:
        response (Response): FastAPI response
        access_token (str | None, optional): token. Defaults to None.
        refresh_token (str | None, optional): token. Defaults to None.
    """
    if access_token:
        response.set_cookie(
            key="token",
            value=access_token,
            httponly=True,
            samesite="strict",
            max_age=ACCESS_TOKEN_EXPIRE_MINUTES * 60,
        )

    if refresh_token:
        response.set_cookie(
            key="refresh",
            value=refresh_token,
            path="/token/refresh",
            httponly=True,
            samesite="strict",
            max_age=REFRESH_TOKEN_EXPIRE_MINUTES * 60,
        )
