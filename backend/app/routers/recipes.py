"""
Recipe routes
"""
import asyncio
import uuid
from typing import Optional

from fastapi import APIRouter, Depends, HTTPException, Response, status
from sqlalchemy import and_, delete, func, or_, select, update
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import load_only, selectinload

from app import serializers
from app.auth import authenticate_access_token, get_current_active_user
from app.constants import DEFAULT_PAGE_SIZE
from app.database import get_database_session, models
from app.utils import (
    get_instance_or_404,
    raise_http_exception_if_user_is_not_owner_or_super,
)

router = APIRouter(
    prefix="/recipes", tags=["recipes"], responses={404: {"description": "Not found"}}
)


##########
# recipes
# /recipes
##########
@router.get("/", response_model=serializers.PaginatedRecipeWithTags)
async def get_recipes(
    session: AsyncSession = Depends(get_database_session),
    current_user: Optional[models.User] = Depends(authenticate_access_token),
    page_size: int = DEFAULT_PAGE_SIZE,
    prev_id: Optional[uuid.UUID] = None,
):
    """Get list of recipes"""
    recipe_query = models.Recipe.paginated_id_query(
        prev_id=prev_id, page_size=page_size
    )
    total_items_query = select(func.count("*")).select_from(models.Recipe)

    if current_user is None or not current_user.active:
        condition = models.Recipe.private.is_(False)
        recipe_query = recipe_query.where(condition)
        total_items_query = total_items_query.where(condition)

    elif current_user.super_user:
        pass

    else:
        condition = or_(
            models.Recipe.created_by_id == current_user.id,
            models.Recipe.private.is_(False),
        )
        recipe_query = recipe_query.where(condition)
        total_items_query = total_items_query.where(condition)

    recipe_query = recipe_query.options(
        selectinload(models.Recipe.tags).options(load_only(models.Tag.id))
    )
    recipe_result, total_count_result = await asyncio.gather(
        session.execute(recipe_query), session.execute(total_items_query)
    )
    return {
        "items": recipe_result.scalars().all(),
        "total_items": total_count_result.scalar(),
        "page_size": page_size,
        "prev_id": prev_id,
    }


@router.post(
    "/",
    response_model=serializers.Recipe,
    responses={409: {"description": "Post caused conflict"}},
    status_code=201,
)
async def create_recipe(
    recipe: serializers.RecipeBase,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    """Add new recipe"""
    new_recipe = models.Recipe(**recipe.dict(), created_by_id=current_user.id)
    try:
        session.add(new_recipe)
        await session.commit()

    except IntegrityError as error:
        await session.rollback()
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=str(error))

    return new_recipe


###########################
# recipe detail
# /recipes/{recipe_id: uuid.UUID}
###########################
@router.get(
    "/{recipe_id}/",
    response_model=serializers.RecipeWithRelations,
)
async def get_recipe_detail(
    recipe_id: uuid.UUID,
    session: AsyncSession = Depends(get_database_session),
    current_user: Optional[models.User] = Depends(authenticate_access_token),
):
    """Get recipe detail"""
    recipe = await get_instance_or_404(
        session,
        models.Recipe,
        options=(
            selectinload(models.Recipe.ingredients),
            selectinload(models.Recipe.tags).options(load_only(models.Tag.id)),
            selectinload(models.Recipe.steps),
        ),
        id=recipe_id,
    )

    if recipe.private:
        raise_http_exception_if_user_is_not_owner_or_super(recipe, user=current_user)

    return recipe


@router.put(
    "/{recipe_id}/",
    responses={
        403: {"description": "Not allowed"},
        409: {"description": "Put caused conflict"},
    },
    response_model=serializers.Recipe,
)
async def edit_recipe(
    recipe_id: uuid.UUID,
    recipe: serializers.RecipeBase,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    """Edit recipe"""
    db_recipe = await get_instance_or_404(session, models.Recipe, id=recipe_id)

    raise_http_exception_if_user_is_not_owner_or_super(db_recipe, user=current_user)

    try:
        await session.execute(
            update(models.Recipe)
            .where(models.Recipe.id == recipe_id)
            .values(**recipe.dict())
        )

    except IntegrityError as error:
        await session.rollback()
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail=str(error)
        ) from error

    return db_recipe


@router.delete(
    "/{recipe_id}/",
    responses={
        403: {"description": "Not allowed"},
        409: {"description": "Put caused conflict"},
    },
    response_class=Response,
    status_code=status.HTTP_204_NO_CONTENT,
)
async def delete_recipe(
    recipe_id: uuid.UUID,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    """Delete recipe"""
    recipe = await get_instance_or_404(session, models.Recipe, id=recipe_id)

    raise_http_exception_if_user_is_not_owner_or_super(recipe, user=current_user)

    try:
        await session.execute(
            delete(models.Recipe).where(models.Recipe.id == recipe_id)
        )

    except IntegrityError as error:
        await session.rollback()
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail=str(error)
        ) from error


#######################################
# ingredients in recipe
# /recipes/{recipe_id: uuid.UUID}/ingredients
#######################################
@router.get(
    "/{recipe_id}/ingredients/",
    response_model=serializers.PaginatedIngredientInRecipe,
)
async def get_ingredients_in_recipe(
    recipe_id: uuid.UUID,
    session: AsyncSession = Depends(get_database_session),
    current_user: Optional[models.User] = Depends(authenticate_access_token),
    prev_id: Optional[uuid.UUID] = None,
    page_size: int = DEFAULT_PAGE_SIZE,
):
    """Get list of ingredients in this recipe"""
    recipe = await get_instance_or_404(
        session,
        models.Recipe,
        id=recipe_id,
    )

    if recipe.private:
        raise_http_exception_if_user_is_not_owner_or_super(recipe, user=current_user)

    ingredient_query = models.IngredientInRecipe.paginate_by_ingredient_id_query(
        recipe.id, prev_id=prev_id, page_size=page_size
    )
    total_count_query = (
        select(func.count("*"))
        .select_from(models.IngredientInRecipe)
        .where(models.IngredientInRecipe.recipe_id == recipe_id)
    )
    ingredients_result, total_count_result = await asyncio.gather(
        session.execute(ingredient_query), session.execute(total_count_query)
    )

    return {
        "items": ingredients_result.scalars().all(),
        "total_items": total_count_result.scalar(),
        "page_size": page_size,
        "prev_id": prev_id,
    }


@router.post(
    "/{recipe_id}/ingredients/",
    response_model=serializers.IngredientInRecipe,
    responses={
        403: {"description": "Not allowed"},
        409: {"description": "Post caused conflict"},
    },
    status_code=201,
)
async def add_ingredient_to_recipe(
    recipe_id: uuid.UUID,
    ingredient_in_recipe: serializers.IngredientInRecipeCreate,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    """Add ingredient to recipe"""
    recipe = await get_instance_or_404(
        session,
        models.Recipe,
        options=(selectinload(models.Recipe.ingredients),),
        id=recipe_id,
    )

    raise_http_exception_if_user_is_not_owner_or_super(recipe, user=current_user)

    try:
        new_ingredient_in_recipe = models.IngredientInRecipe(
            **ingredient_in_recipe.dict(), created_by_id=current_user.id
        )
        recipe.ingredients.append(new_ingredient_in_recipe)
        await session.commit()

    except IntegrityError as error:
        await session.rollback()
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail=str(error)
        ) from error

    return new_ingredient_in_recipe


################################################################
# ingredient in recipe detail
# /{recipe_id: uuid.UUID}/ingredients/{ingredient_id: uuid.UUID}
################################################################
@router.get(
    "/{recipe_id}/ingredients/{ingredient_id}/",
    response_model=serializers.IngredientInRecipe,
)
async def get_ingredient_in_recipe_detail(
    recipe_id: uuid.UUID,
    ingredient_id: uuid.UUID,
    session: AsyncSession = Depends(get_database_session),
    current_user: Optional[models.User] = Depends(authenticate_access_token),
):
    """Get Ingredient In Recipe detail"""
    ingredient_in_recipe, recipe = await asyncio.gather(
        get_instance_or_404(
            session,
            models.IngredientInRecipe,
            recipe_id=recipe_id,
            ingredient_id=ingredient_id,
        ),
        get_instance_or_404(
            session,
            models.Recipe,
            id=recipe_id,
        ),
    )

    if recipe.private:
        raise_http_exception_if_user_is_not_owner_or_super(recipe, user=current_user)

    return ingredient_in_recipe


@router.put(
    "/{recipe_id}/ingredients/{ingredient_id}/",
    responses={
        403: {"description": "Not allowed"},
        409: {"description": "PUT caused conflict"},
    },
    response_model=serializers.IngredientInRecipe,
)
async def edit_ingredient_in_recipe(
    recipe_id: uuid.UUID,
    ingredient_id: uuid.UUID,
    ingredient_in_recipe: serializers.IngredientInRecipeBase,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    """Edit Ingredient In Recipe"""
    db_ingredient_in_recipe, recipe = await asyncio.gather(
        get_instance_or_404(
            session,
            models.IngredientInRecipe,
            recipe_id=recipe_id,
            ingredient_id=ingredient_id,
        ),
        get_instance_or_404(
            session,
            models.Recipe,
            id=recipe_id,
        ),
    )

    raise_http_exception_if_user_is_not_owner_or_super(recipe, user=current_user)

    try:
        query = (
            update(models.IngredientInRecipe)
            .where(
                and_(
                    models.IngredientInRecipe.recipe_id == recipe_id,
                    models.IngredientInRecipe.ingredient_id == ingredient_id,
                )
            )
            .values(**ingredient_in_recipe.dict())
        )
        await session.execute(query)

    except IntegrityError as error:
        await session.rollback()
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail=str(error)
        ) from error

    return db_ingredient_in_recipe


@router.delete(
    "/{recipe_id}/ingredients/{ingredient_id}/",
    status_code=204,
    responses={
        403: {"description": "Not allowed"},
        409: {"description": "DELETE caused conflict"},
    },
)
async def remove_ingredient_from_recipe(
    recipe_id: uuid.UUID,
    ingredient_id: uuid.UUID,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    """Remove ingredient from recipe"""
    recipe = await get_instance_or_404(session, models.Recipe, id=recipe_id)

    raise_http_exception_if_user_is_not_owner_or_super(recipe, user=current_user)

    try:
        query = delete(models.IngredientInRecipe).where(
            and_(
                models.IngredientInRecipe.recipe_id == recipe_id,
                models.IngredientInRecipe.ingredient_id == ingredient_id,
            )
        )
        result = await session.execute(query)

    except IntegrityError:
        await session.rollback()

    if result.rowcount == 0:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Ingredient not found for that recipe",
        )

    return


#########################
# recipe steps
# /{recipe_id: uuid.UUID}/steps
#########################
@router.get(
    "/{recipe_id}/steps/",
    response_model=serializers.PaginatedStep,
)
async def get_recipe_steps(
    recipe_id: uuid.UUID,
    session: AsyncSession = Depends(get_database_session),
    current_user: Optional[models.User] = Depends(authenticate_access_token),
    prev_order: Optional[int] = None,
    page_size: int = DEFAULT_PAGE_SIZE,
):
    """Get steps in recipe"""
    recipe = await get_instance_or_404(
        session,
        models.Recipe,
        id=recipe_id,
    )

    if recipe.private:
        raise_http_exception_if_user_is_not_owner_or_super(recipe, user=current_user)

    steps_query = models.Step.paginate_by_order_query(
        recipe.id, prev_order=prev_order, page_size=page_size
    )
    total_count_query = (
        select(func.count("*"))
        .select_from(models.Step)
        .where(models.Step.recipe_id == recipe.id)
    )
    steps_result, total_count_result = await asyncio.gather(
        session.execute(steps_query), session.execute(total_count_query)
    )
    return {
        "items": steps_result.scalars().all(),
        "total_items": total_count_result.scalar(),
        "page_size": page_size,
        "prev_order": prev_order,
    }


@router.post(
    "/{recipe_id}/steps/",
    response_model=serializers.Step,
    responses={
        403: {"description": "Not allowed"},
        409: {"description": "Post caused conflict"},
    },
    status_code=201,
)
async def add_step_to_recipe(
    recipe_id: uuid.UUID,
    step: serializers.StepBase,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    """Add step to recipe"""
    recipe = await get_instance_or_404(
        session,
        models.Recipe,
        options=(selectinload(models.Recipe.steps),),
        id=recipe_id,
    )

    raise_http_exception_if_user_is_not_owner_or_super(recipe, user=current_user)

    query = (
        select(func.count("*"))
        .select_from(models.Step)
        .where(models.Step.recipe_id == recipe_id)
    )
    result = await session.execute(query)
    order = result.scalar() + 1

    try:
        new_step = models.Step(
            instruction=step.instruction,
            order=order,
            created_by_id=current_user.id,
        )
        recipe.steps.append(new_step)
        await session.commit()

    except IntegrityError as error:
        await session.rollback()
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail=str(error)
        ) from error

    return new_step


######################################
# recipe step detail
# /{recipe_id: uuid.UUID}/steps/{order: int}
######################################


@router.get(
    "/{recipe_id}/steps/{order}/",
    response_model=serializers.Step,
)
async def get_step_detail(
    recipe_id: uuid.UUID,
    order: int,
    session: AsyncSession = Depends(get_database_session),
    current_user: Optional[models.User] = Depends(authenticate_access_token),
):
    """get step detail"""
    step, recipe = await asyncio.gather(
        get_instance_or_404(session, models.Step, recipe_id=recipe_id, order=order),
        get_instance_or_404(
            session,
            models.Recipe,
            id=recipe_id,
        ),
    )

    if recipe.private:
        raise_http_exception_if_user_is_not_owner_or_super(recipe, user=current_user)

    return step


@router.put(
    "/{recipe_id}/steps/{order}/",
    response_model=serializers.Step,
    responses={
        403: {"description": "Not allowed"},
        409: {"description": "Post caused conflict"},
    },
)
async def edit_step(
    recipe_id: uuid.UUID,
    order: int,
    step: serializers.StepBase,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    """Edit step"""
    db_step, recipe = await asyncio.gather(
        get_instance_or_404(session, models.Step, recipe_id=recipe_id, order=order),
        get_instance_or_404(
            session,
            models.Recipe,
            id=recipe_id,
        ),
    )

    raise_http_exception_if_user_is_not_owner_or_super(recipe, user=current_user)

    try:
        query = (
            update(models.Step)
            .where(
                and_(
                    models.Step.recipe_id == recipe_id,
                    models.Step.order == order,
                )
            )
            .values(**step.dict())
        )
        await session.execute(query)

    except IntegrityError as error:
        await session.rollback()
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail=str(error)
        ) from error

    return db_step


@router.delete(
    "/{recipe_id}/steps/{order}/",
    status_code=204,
    responses={
        403: {"description": "Not allowed"},
        409: {"description": "Delete caused conflict"},
    },
)
async def delete_step(
    recipe_id: uuid.UUID,
    order: int,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    """Remove step from recipe. Only the last step can be deleted"""
    recipe = await get_instance_or_404(session, models.Recipe, id=recipe_id)

    raise_http_exception_if_user_is_not_owner_or_super(recipe, user=current_user)

    query = (
        select(func.count("*"))
        .select_from(models.Step)
        .where(models.Step.recipe_id == recipe_id)
    )
    result = await session.execute(query)
    step_count = result.scalar()
    if order != step_count:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=(
                "Only the last step in the recipe can be deleted. Delete step at "
                f"order {step_count}"
            ),
        )

    try:
        query = delete(models.Step).where(
            and_(
                models.Step.recipe_id == recipe_id,
                models.Step.order == order,
            )
        )
        result = await session.execute(query)

    except IntegrityError:
        await session.rollback()


########################
# recipe tags
# /{recipe_id: uuid.UUID}/tags
########################
@router.get(
    "/{recipe_id}/tags/",
    response_model=serializers.PaginatedTag,
)
async def get_tags_on_recipe(
    recipe_id: uuid.UUID,
    session: AsyncSession = Depends(get_database_session),
    current_user: Optional[models.User] = Depends(authenticate_access_token),
    prev_id: Optional[uuid.UUID] = None,
    page_size: int = DEFAULT_PAGE_SIZE,
):
    """Get list of tags for this recipe"""
    recipe = await get_instance_or_404(
        session,
        models.Recipe,
        id=recipe_id,
    )
    if recipe.private:
        raise_http_exception_if_user_is_not_owner_or_super(recipe, user=current_user)

    tags_query = (
        models.Tag.paginated_id_query(prev_id=prev_id, page_size=page_size)
        .join(models.recipe_tags, models.Tag.id == models.recipe_tags.c["tag_id"])
        .where(models.recipe_tags.c["recipe_id"] == recipe.id)
    )

    total_count_result = (
        select(func.count("*"))
        .select_from(models.recipe_tags)
        .where(models.recipe_tags.c["recipe_id"] == recipe.id)
    )
    tags_result, total_count_result = await asyncio.gather(
        session.execute(tags_query), session.execute(total_count_result)
    )
    return {
        "items": tags_result.scalars().all(),
        "total_items": total_count_result.scalar(),
        "page_size": page_size,
        "prev_id": prev_id,
    }


@router.post(
    "/{recipe_id}/tags/",
    response_model=serializers.Tag,
    responses={409: {"detail": "Post caused conflict"}, 403: {"detail": "Not allowed"}},
    status_code=201,
)
async def add_tag_to_recipe(
    recipe_id: uuid.UUID,
    tag: serializers.AddTagToRecipe,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    """Add tag to recipe"""
    recipe = await get_instance_or_404(
        session,
        models.Recipe,
        options=(selectinload(models.Recipe.tags),),
        id=recipe_id,
    )

    raise_http_exception_if_user_is_not_owner_or_super(recipe, user=current_user)

    query = select(models.Tag).where(models.Tag.id == tag.id)
    result = await session.execute(query)
    if (db_tag := result.scalar()) is None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="tag id was invalid",
        )

    try:
        recipe.tags.append(db_tag)
        await session.commit()

    except IntegrityError:
        await session.rollback()
        # if it already exists do nothing

    return db_tag


#################################
# recipe tag detail
# /{recipe_id: uuid.UUID}/tags/{tag_id}
#################################


@router.delete(
    "/{recipe_id}/tags/{tag_id}/",
    responses={409: {"detail": "Post caused conflict"}, 403: {"detail": "Not allowed"}},
    status_code=204,
)
async def remove_tag_from_recipe(
    recipe_id: uuid.UUID,
    tag_id: uuid.UUID,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    """Remove tag from recipe"""
    recipe = await get_instance_or_404(
        session,
        models.Recipe,
        options=(selectinload(models.Recipe.tags),),
        id=recipe_id,
    )

    raise_http_exception_if_user_is_not_owner_or_super(recipe, user=current_user)

    tag = await get_instance_or_404(session, models.Tag, id=tag_id)

    try:
        recipe.tags.remove(tag)
        await session.commit()
    except (ValueError, IntegrityError):
        # if tag is not on recipe do nothing
        await session.rollback()
        pass

    return
