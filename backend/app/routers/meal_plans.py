import asyncio
import uuid
from typing import Optional

from fastapi import APIRouter, Depends, HTTPException, Response, status
from sqlalchemy import delete, update
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.sql.expression import select
from sqlalchemy.sql.functions import func

from app import serializers
from app.auth import get_current_active_user
from app.constants import DEFAULT_PAGE_SIZE
from app.database import get_database_session, models
from app.utils import get_instance_or_404

router = APIRouter(
    prefix="/meal-plans",
    tags=["meal-plans"],
    responses={404: {"description": "Not found"}},
)


###############
# # /meal-plans
###############
@router.get("/", response_model=serializers.PaginatedMealPlan)
async def get_meal_plans(
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
    prev_id: Optional[uuid.UUID] = None,
    page_size: int = DEFAULT_PAGE_SIZE,
):
    """Get list of meal plans for current logged in user"""
    condition = models.MealPlan.created_by_id == current_user.id
    meal_plan_query = (
        models.MealPlan.paginated_id_query(prev_id=prev_id, page_size=page_size)
        .where(condition)
        .order_by(models.MealPlan.date)
    )
    total_count_query = (
        select(func.count("*")).select_from(models.MealPlan).where(condition)
    )
    meal_plan_result, total_count_result = await asyncio.gather(
        session.execute(meal_plan_query), session.execute(total_count_query)
    )
    return {
        "items": meal_plan_result.scalars().all(),
        "total_items": total_count_result.scalar(),
        "page_size": page_size,
        "prev_id": prev_id,
    }


@router.post(
    "/",
    response_model=serializers.MealPlan,
    responses={409: {"description": "Post caused conflict"}},
    status_code=status.HTTP_201_CREATED,
)
async def create_meal_plan(
    meal_plan: serializers.MealPlanCreate,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
) -> models.MealPlan:
    """Create new meal plan"""
    new_meal_plan = models.MealPlan(**meal_plan.dict(), created_by_id=current_user.id)

    try:
        session.add(new_meal_plan)
        await session.commit()

    except IntegrityError as error:
        await session.rollback()
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=str(error))

    return new_meal_plan


##############################
# # /meal-plans/{meal-plan-id}
##############################
@router.get("/{meal_plan_id}/", response_model=serializers.MealPlan)
async def get_meal_plan_detail(
    meal_plan_id: uuid.UUID,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
) -> models.MealPlan:
    """Get meal plan detail"""
    return await get_instance_or_404(
        session, models.MealPlan, id=meal_plan_id, created_by_id=current_user.id
    )


@router.put(
    "/{meal_plan_id}/",
    response_model=serializers.MealPlan,
    responses={
        403: {"description": "Not allowed"},
        409: {"description": "Put caused conflict"},
    },
)
async def edit_meal_plan(
    meal_plan_id: uuid.UUID,
    meal_plan: serializers.MealPlanBase,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
) -> models.MealPlan:
    """Edit meal plan"""
    db_meal_plan = await get_instance_or_404(session, models.MealPlan, id=meal_plan_id)

    if not current_user.super_user and db_meal_plan.created_by_id != current_user.id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You are not allowed to edit ingredients you did not create",
        )

    try:
        await session.execute(
            update(models.MealPlan)
            .where(models.MealPlan.id == meal_plan_id)
            .values(**meal_plan.dict())
        )

    except IntegrityError as error:
        await session.rollback()
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail=str(error)
        ) from error

    return db_meal_plan


@router.delete(
    "/{meal_plan_id}/",
    responses={409: {"description": "Put caused conflict"}},
    status_code=status.HTTP_204_NO_CONTENT,
    response_class=Response,
)
async def delete_meal_plan(
    meal_plan_id: uuid.UUID,
    session: AsyncSession = Depends(get_database_session),
    current_user: serializers.UserDBModel = Depends(get_current_active_user),
):
    """Delete meal plan"""
    db_meal_plan = await get_instance_or_404(session, models.MealPlan, id=meal_plan_id)

    if not current_user.super_user and db_meal_plan.created_by_id != current_user.id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You are not allowed to delete ingredients you did not create",
        )

    try:
        await session.execute(
            delete(models.MealPlan).where(models.MealPlan.id == meal_plan_id)
        )

    except IntegrityError as error:
        await session.rollback()
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail=str(error)
        ) from error
