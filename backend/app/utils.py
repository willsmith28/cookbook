from collections.abc import Iterable
from typing import Type, TypeVar

from fastapi import HTTPException, status
from sqlalchemy import and_, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm.exc import NoResultFound

from app.constants import DETAIL_403, DETAIL_404
from app.database import models

AnyModel = TypeVar(
    "AnyModel",
    models.User,
    models.IngredientInRecipe,
    models.Recipe,
    models.Ingredient,
    models.Tag,
    models.Step,
    models.MealPlan,
)


def snake_to_camel_case(value: str) -> str:
    """convert snake_case to camelCase

    Args:
        value (str): the snake_case string

    Returns:
        str: the camelCase string
    """
    first_word, *others = value.split("_")
    return "".join((first_word.lower(), *(other_word.title() for other_word in others)))


async def get_instance_or_404(
    session: AsyncSession, model: Type[AnyModel], options: Iterable = (), **kwargs
) -> AnyModel:
    """executes a select on the given model with kwargs as where clase model.key == value

    Args:
        session (AsyncSession): the active session
        model (Type[AnyModel]): a model from app.database.models
        **kwargs: the arguments of the where clause model.key == value
            anded together if multiple are provided

    Raises:
        TypeError: at least one kwarg must be provided
        HTTPException: 404 The resource you requested could not be found

    Returns:
        model provided: the instance of the model requested
    """
    if not kwargs:
        raise TypeError("app.utils.get_instance_or_404 requries at least one kwarg")

    elif len(kwargs) == 1:
        ((key, value),) = kwargs.items()
        where_clause = getattr(model, key) == value

    else:
        where_clause = and_(
            *(getattr(model, key) == value for key, value in kwargs.items())
        )

    query = select(model).where(where_clause)

    if options:
        query = query.options(*options)

    result = await session.execute(query)

    try:
        return result.scalar_one()

    except NoResultFound as error:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=DETAIL_404,
        ) from error


def raise_http_exception_if_user_is_not_owner_or_super(
    recipe: models.Recipe,
    user: models.User | None = None,
):
    if (
        user is None
        or not user.active
        or (user.id != recipe.created_by_id and not user.super_user)
    ):
        status_code, detail = (
            (status.HTTP_404_NOT_FOUND, DETAIL_404)
            if recipe.private
            else (status.HTTP_403_FORBIDDEN, DETAIL_403)
        )

        raise HTTPException(status_code=status_code, detail=detail)
