"""Authentication Code"""
import datetime
from typing import Optional

from fastapi import Cookie, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import ExpiredSignatureError, JWTError, jwt
from passlib.context import CryptContext
from sqlalchemy import and_, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm.exc import NoResultFound

from app import serializers
from app.config import SECRET_KEY
from app.constants import ACCESS_TOKEN_EXPIRE_MINUTES, ALGORITHM, ISSUER
from app.database import get_database_session, models

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token", auto_error=False)

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


class Token(serializers.CustomBaseModel):
    access_token: str
    refresh_token: str
    token_type: str


class RefreshTokenResponse(serializers.CustomBaseModel):
    access_token: str
    token_type: str


class TokenData(serializers.CustomBaseModel):
    iat: datetime.datetime
    exp: datetime.datetime
    iss: str
    sub: str
    username: str
    email: str
    active: bool
    super_user: bool


class RefreshToken(serializers.CustomBaseModel):
    refresh: str


def verify_password(plain_password: str, hashed_password: str) -> bool:
    """verify password and hashed_password matches using application CryptContext

    Args:
        plain_password (str): plaintext password
        hashed_password (str): hashed password

    Returns:
        bool: true if passwords match
    """
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password: str) -> str:
    """hash password with application CryptContext

    Args:
        password (str): plaintext password

    Returns:
        str: hashed password
    """
    return pwd_context.hash(password)


def create_token(
    user: models.User, expires_delta: datetime.timedelta | None = None
) -> str:
    """Create JWT

    Args:
        user (models.User): User to create JWT from
        expires_delta (datetime.timedelta | None, optional): how long the JWT should
            be valid.

    Returns:
        str: JWT
    """
    now = datetime.datetime.utcnow()

    expire = now + (
        expires_delta
        if expires_delta
        else datetime.timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    )

    token = TokenData(
        exp=expire,
        iat=now,
        iss=ISSUER,
        sub=str(user.id),
        username=user.username,
        email=user.email,
        active=user.active,
        super_user=user.super_user,
    )

    return jwt.encode(token.dict(), SECRET_KEY, algorithm=ALGORITHM)


async def get_user(session: AsyncSession, **kwargs) -> models.User:
    """query DB for username and return user model if exists

    Args:
        session (AsyncSession): database session
        **kwargs: search for models.User.key == value anded together

    Raises:
        sqlalchemy.orm.exc.NoResultFound: no user could be found
    Returns:
        models.User: User instance
    """
    if not kwargs:
        raise ValueError("either user_id or username must be passed to get_user")

    if len(kwargs) > 1:
        where_clause = and_(
            **(getattr(models.User, key) == value for key, value in kwargs.items())
        )

    else:
        ((key, value),) = kwargs.items()
        where_clause = getattr(models.User, key) == value

    query = select(models.User).where(where_clause)
    result = await session.execute(query)
    return result.scalar_one()


async def authenticate_user(
    session: AsyncSession, username: str, password: str
) -> models.User:
    """Authenticate username and password

    Args:
        session (AsyncSession): datbase session
        username (str): username
        password (str): plaintext password

    Raises:
        HTTPException: Could not validate credentials either user not found or password
            not correct

    Returns:
        models.User: User instance of authenticated user
    """
    auth_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        user = await get_user(session, username=username)

    except NoResultFound as error:
        raise auth_exception from error

    if not verify_password(password, user.hashed_password):
        raise auth_exception

    return user


async def authenticate_access_token(
    session: AsyncSession = Depends(get_database_session),
    token: Optional[str] = Depends(oauth2_scheme),
    cookie_token: Optional[str] = Cookie(None, alias="token"),
) -> Optional[models.User]:
    """Authenticates JWT Bearer token and returns user it belongs to or None
    if no token was provided

    Args:
        session (AsyncSession): database session
        token (str): user token from header.
        cookie_token (str): user token from cookie.

    Raises:
        HTTPException: Could not validate token.

    Returns:
        models.User | None: User instance
    """
    if token is None and cookie_token is None:
        return

    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate token.",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(
            token or cookie_token, SECRET_KEY, algorithms=[ALGORITHM], issuer=ISSUER
        )

    except (JWTError, ExpiredSignatureError) as error:
        raise credentials_exception from error

    user_id: str = payload.get("sub")
    if not user_id:
        raise credentials_exception

    try:
        return await get_user(session, id=user_id)

    except NoResultFound as error:
        raise credentials_exception from error


async def get_current_active_user(
    current_user: models.User = Depends(authenticate_access_token),
) -> serializers.UserDBModel:
    """Gets the current user if the user account is active

    Args:
        current_user (serializers.UserDBModel): User associated to token.

    Raises:
        HTTPException: Your account has been deactivated

    Returns:
        serializers.UserDBModel: pydantic serializers.UserDBModel instance
    """
    if current_user is None or not current_user.active:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate token",
        )

    return current_user
