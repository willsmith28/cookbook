from typing import Generator

from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

from app.config import DATABASE_URL

engine = create_async_engine(DATABASE_URL, echo=False)
Session = sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)


async def get_database_session() -> Generator[AsyncSession, None, None]:
    """Creates and begins an async database session.
    Because the session.begin() has been called only session.flush()
    should be called when using the session.


    Yields:
        Generator[AsyncSession, None, None]: active database session
    """
    session: AsyncSession
    async with Session() as session:
        yield session
