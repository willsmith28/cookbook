"""
SQLAlchemy models
"""
import uuid

import sqlalchemy as sa
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import registry, relationship

from app.constants import DEFAULT_PAGE_SIZE
from app.database.mixins import CreatedByMixin, PaginatedMixin, TimeStampMixin

mapper_registry = registry()

recipe_tags = sa.Table(
    "recipe_tags",
    mapper_registry.metadata,
    sa.Column(
        "recipe_id",
        postgresql.UUID(as_uuid=True),
        sa.ForeignKey("recipe.id", ondelete="CASCADE"),
        primary_key=True,
        nullable=False,
    ),
    sa.Column(
        "tag_id",
        postgresql.UUID(as_uuid=True),
        sa.ForeignKey("tag.id"),
        primary_key=True,
        nullable=False,
    ),
)


@mapper_registry.mapped
class User(TimeStampMixin):
    __tablename__ = "user"
    id = sa.Column(postgresql.UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    username = sa.Column(sa.String, nullable=False, unique=True)
    email = sa.Column(sa.String, nullable=False, unique=True)
    hashed_password = sa.Column(sa.String, nullable=False)
    active = sa.Column(sa.Boolean, nullable=False, default=True)
    super_user = sa.Column(sa.Boolean, nullable=False, default=False)
    refresh_token = sa.Column(sa.String)


@mapper_registry.mapped
class IngredientInRecipe(TimeStampMixin, CreatedByMixin):
    __tablename__ = "ingredient_in_recipe"
    recipe_id = sa.Column(
        postgresql.UUID(as_uuid=True),
        sa.ForeignKey("recipe.id", ondelete="CASCADE"),
        primary_key=True,
    )
    ingredient_id = sa.Column(
        postgresql.UUID(as_uuid=True),
        sa.ForeignKey("ingredient.id"),
        primary_key=True,
    )
    amount = sa.Column(sa.DECIMAL(precision=6, scale=2), nullable=True)
    unit = sa.Column(sa.String, nullable=True)
    comment = sa.Column(sa.String, nullable=True)
    other = sa.Column(sa.String, nullable=True)
    ingredient = relationship(
        "Ingredient", back_populates="used_in_recipes", lazy="raise"
    )
    recipe = relationship("Recipe", back_populates="ingredients", lazy="raise")

    @classmethod
    def paginate_by_ingredient_id_query(
        cls,
        recipe_id: uuid.UUID,
        prev_id: uuid.UUID | None = None,
        page_size: int = DEFAULT_PAGE_SIZE,
    ):
        query = (
            sa.select(cls)
            .order_by(cls.ingredient_id)
            .limit(page_size)
            .where(cls.recipe_id == recipe_id)
        )

        if prev_id:
            query = query.where(cls.ingredient_id > prev_id)

        return query


@mapper_registry.mapped
class Recipe(TimeStampMixin, CreatedByMixin, PaginatedMixin):
    __tablename__ = "recipe"
    id = sa.Column(postgresql.UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = sa.Column(sa.String, nullable=False)
    description = sa.Column(sa.String, nullable=False)
    servings = sa.Column(sa.String, nullable=False)
    cook_time = sa.Column(sa.String, nullable=False)
    notes = sa.Column(sa.String, nullable=True)
    private = sa.Column(sa.Boolean, nullable=False, default=False)

    ingredients = relationship(
        "IngredientInRecipe",
        back_populates="recipe",
        cascade="all, delete",
        passive_deletes=True,
        lazy="raise",
    )
    steps = relationship(
        "Step",
        back_populates="recipe",
        order_by="Step.order",
        cascade="all, delete",
        passive_deletes=True,
        lazy="raise",
    )
    used_as_ingredient = relationship(
        "Ingredient",
        uselist=False,
        back_populates="recipe",
        cascade="all, delete",
        passive_deletes=True,
        lazy="raise",
    )
    tags = relationship(
        "Tag",
        secondary=recipe_tags,
        cascade="all, delete",
        passive_deletes=True,
        lazy="raise",
    )
    meal_plans = relationship(
        "MealPlan",
        back_populates="recipe",
        order_by="MealPlan.date",
        cascade="all, delete",
        passive_deletes=True,
        lazy="raise",
    )


@mapper_registry.mapped
class Ingredient(TimeStampMixin, CreatedByMixin, PaginatedMixin):
    __tablename__ = "ingredient"
    id = sa.Column(postgresql.UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = sa.Column(sa.String, nullable=False, unique=True)
    recipe_id = sa.Column(
        postgresql.UUID(as_uuid=True),
        sa.ForeignKey("recipe.id", ondelete="CASCADE"),
        nullable=True,
        unique=True,
    )
    recipe = relationship("Recipe", back_populates="used_as_ingredient", lazy="raise")
    used_in_recipes = relationship(
        "IngredientInRecipe", back_populates="ingredient", lazy="raise"
    )


@mapper_registry.mapped
class Tag(TimeStampMixin, CreatedByMixin, PaginatedMixin):
    __tablename__ = "tag"
    id = sa.Column(postgresql.UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = sa.Column(sa.String, nullable=False, unique=True)
    type = sa.Column(sa.String, nullable=False)


@mapper_registry.mapped
class Step(TimeStampMixin, CreatedByMixin):
    __tablename__ = "step"
    recipe_id = sa.Column(
        postgresql.UUID(as_uuid=True),
        sa.ForeignKey("recipe.id", ondelete="CASCADE"),
        primary_key=True,
    )
    order = sa.Column(sa.INTEGER, primary_key=True)
    instruction = sa.Column(sa.String, nullable=False)
    recipe = relationship("Recipe", back_populates="steps", lazy="raise")

    @classmethod
    def paginate_by_order_query(
        cls,
        recipe_id,
        prev_order: int | None = None,
        page_size: int = DEFAULT_PAGE_SIZE,
    ):
        query = (
            sa.select(cls)
            .order_by(cls.order)
            .limit(page_size)
            .where(cls.recipe_id == recipe_id)
        )

        if prev_order:
            query = query.where(cls.order > prev_order)

        return query


@mapper_registry.mapped
class MealPlan(TimeStampMixin, CreatedByMixin, PaginatedMixin):
    __tablename__ = "meal_plan"
    id = sa.Column(postgresql.UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    date = sa.Column(sa.DATE, nullable=False)
    type = sa.Column(sa.String, nullable=False)
    cooked = sa.Column(sa.Boolean, nullable=False, default=False)
    private = sa.Column(sa.Boolean, nullable=False, default=True)
    recipe_id = sa.Column(
        postgresql.UUID(as_uuid=True),
        sa.ForeignKey("recipe.id", ondelete="CASCADE"),
        nullable=False,
    )
    recipe = relationship("Recipe", back_populates="meal_plans", lazy="raise")


@mapper_registry.mapped
class IngredientPhraseTaggingHistory(TimeStampMixin, PaginatedMixin):
    __tablename__ = "ingredient_phrase_tagging_history"
    id = sa.Column(postgresql.UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    input = sa.Column(sa.String, nullable=False)
    name = sa.Column(sa.String, nullable=False)
    qty = sa.Column(sa.DECIMAL(precision=6, scale=2), nullable=False)
    range_end = sa.Column(sa.DECIMAL(precision=6, scale=2), nullable=False, default=0.0)
    unit = sa.Column(sa.String)
    comment = sa.Column(sa.String)
