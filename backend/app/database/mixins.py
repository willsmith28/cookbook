import datetime
import uuid

import sqlalchemy as sa
from sqlalchemy.dialects import postgresql
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import relationship

from app.constants import DEFAULT_PAGE_SIZE


class TimeStampMixin:
    created_at = sa.Column(
        sa.DateTime,
        nullable=False,
        default=datetime.datetime.utcnow,
    )
    last_updated_at = sa.Column(
        sa.DateTime,
        nullable=False,
        default=datetime.datetime.utcnow,
        onupdate=datetime.datetime.utcnow,
    )


class CreatedByMixin:
    @declared_attr
    def created_by_id(cls):
        return sa.Column(
            postgresql.UUID(as_uuid=True),
            sa.ForeignKey("user.id", name="created_by_id"),
            nullable=False,
        )

    @declared_attr
    def created_by(cls):
        return relationship("User", lazy="raise")


class PaginatedMixin:
    @classmethod
    def paginated_id_query(
        cls, prev_id: uuid.UUID | None = None, page_size: int = DEFAULT_PAGE_SIZE
    ):
        query = sa.select(cls).order_by(cls.id).limit(page_size)

        if prev_id:
            query = query.where(cls.id > prev_id)

        return query
