"""
serializer models
"""
import datetime
import uuid
from typing import Optional

from pydantic import BaseModel, EmailStr, condecimal, constr

from app import constants
from app.utils import snake_to_camel_case


class CustomBaseModel(BaseModel):
    class Config:
        alias_generator = snake_to_camel_case
        allow_population_by_field_name = True


######
# User
######
class User(CustomBaseModel):
    username: constr(strip_whitespace=True, min_length=3)
    email: EmailStr


class UserSignUp(User):
    password: constr(min_length=6)


class UserDBModel(User):
    id: uuid.UUID
    hashed_password: str
    active: bool = True
    super_user: bool = False
    created_at: datetime.datetime
    last_updated_at: datetime.datetime

    class Config:
        orm_mode = True


############
# Ingredient
############
class IngredientBase(CustomBaseModel):
    name: str
    recipe_id: Optional[uuid.UUID] = None


class Ingredient(IngredientBase):
    id: uuid.UUID
    created_at: datetime.datetime
    last_updated_at: datetime.datetime

    class Config:
        orm_mode = True


class PaginatedIngredient(CustomBaseModel):
    items: list[Ingredient]
    total_items: int
    page_size: int
    prev_id: Optional[uuid.UUID] = None


#####
# Tag
#####
class TagBase(CustomBaseModel):
    name: str
    type: constants.TagTypes


class Tag(TagBase):
    id: uuid.UUID
    created_at: datetime.datetime
    last_updated_at: datetime.datetime

    class Config:
        orm_mode = True


class PaginatedTag(CustomBaseModel):
    items: list[Tag]
    total_items: int
    page_size: int
    prev_id: Optional[uuid.UUID] = None


class AddTagToRecipe(CustomBaseModel):
    id: uuid.UUID


####################
# IngredientInRecipe
####################
class IngredientInRecipeBase(CustomBaseModel):
    amount: Optional[condecimal(gt=0, max_digits=6, decimal_places=2)]
    unit: Optional[constants.IngredientUnits]
    comment: Optional[str]
    other: Optional[str]


class IngredientInRecipeCreate(IngredientInRecipeBase):
    ingredient_id: uuid.UUID


class IngredientInRecipe(IngredientInRecipeCreate):
    recipe_id: uuid.UUID
    created_at: datetime.datetime
    last_updated_at: datetime.datetime

    class Config:
        orm_mode = True


class PaginatedIngredientInRecipe(CustomBaseModel):
    items: list[IngredientInRecipe]
    total_items: int
    page_size: int
    prev_id: Optional[uuid.UUID] = None


################################
# IngredientPhraseTaggingHistory
################################
class IngredientPhraseTaggingHistoryBase(BaseModel):
    input: str
    name: str
    qty: condecimal(gt=0, max_digits=6, decimal_places=2)
    range_end: condecimal(gt=0, max_digits=6, decimal_places=2)
    unit: Optional[str]
    comment: Optional[str]


class IngredientPhraseTaggingHistory(IngredientPhraseTaggingHistoryBase):
    id: uuid.UUID
    created_at: datetime.datetime
    last_updated_at: datetime.datetime


######
# Step
######
class StepBase(CustomBaseModel):
    instruction: str


class Step(StepBase):
    order: int
    recipe_id: uuid.UUID
    created_at: datetime.datetime
    last_updated_at: datetime.datetime

    class Config:
        orm_mode = True


class PaginatedStep(CustomBaseModel):
    items: list[Step]
    total_items: int
    page_size: int
    prev_order: Optional[int] = None


########
# Recipe
########
class TagId(CustomBaseModel):
    id: uuid.UUID

    class Config:
        orm_mode = True


class RecipeBase(CustomBaseModel):
    name: str
    description: str
    servings: str
    cook_time: str
    notes: Optional[str]
    private: bool = False


class Recipe(RecipeBase):
    id: uuid.UUID
    created_at: datetime.datetime
    last_updated_at: datetime.datetime
    created_by_id: uuid.UUID

    class Config:
        orm_mode = True


class RecipeWithTags(Recipe):
    tags: list[TagId]

    class Config:
        orm_mode = True


class PaginatedRecipeWithTags(CustomBaseModel):
    items: list[RecipeWithTags]
    total_items: int
    page_size: int


class RecipeWithRelations(RecipeWithTags):
    ingredients: list[IngredientInRecipe]
    steps: list[Step]

    class Config:
        orm_mode = True


##########
# MealPlan
##########
class MealPlanBase(CustomBaseModel):
    date: datetime.date
    type: constants.MealPlanTypes
    cooked: bool = False
    private: bool = True


class MealPlanCreate(MealPlanBase):
    recipe_id: uuid.UUID


class MealPlan(MealPlanBase):
    id: uuid.UUID
    created_at: datetime.datetime
    last_updated_at: datetime.datetime
    created_by_id: uuid.UUID

    class Config:
        orm_mode = True


class PaginatedMealPlan(CustomBaseModel):
    items: list[MealPlan]
    total_items: int
    page_size: int
