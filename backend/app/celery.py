import enum

import celery

from app.config import CELERY_BROKER_URL, CELERY_RESULT_BACKEND


class Tasks(str, enum.Enum):
    add = "add"
    tag_ingredient_phrase = "app.tasks.recipes.tag_ingredients_test"


app = celery.Celery(
    __name__,
    broker=CELERY_BROKER_URL,
    backend=CELERY_RESULT_BACKEND,
)
