import argparse
import getpass

from pydantic import ValidationError
from sqlalchemy import create_engine
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import sessionmaker

from app import serializers
from app.auth import get_password_hash
from app.config import DATABASE_URL
from app.database import models


def main(
    username: str,
    email: str,
    password: str,
):
    engine = create_engine(DATABASE_URL.replace("+asyncpg", ""), future=True)
    Session = sessionmaker(engine, future=True)

    try:
        super_user = serializers.UserSignUp(
            username=username, email=email, password=password
        ).dict()

    except ValidationError as e:
        print(f"{str(e).split('Traceback')[0]}")
        exit(1)

    super_user["hashed_password"] = get_password_hash(super_user.pop("password"))

    try:
        with Session.begin() as session:
            session.add(models.User(**super_user, super_user=True))

    except IntegrityError:
        print("The user already exists")
        exit(1)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--username", help="username for superuser account")
    parser.add_argument("-e", "--email", help="email for superuser account")
    parser.add_argument("-p", "--password", help="password for superuser account")
    args = parser.parse_args()
    if args.password is None:
        password = getpass.getpass("Enter password: ")
        confirm_password = getpass.getpass("Confirm password: ")
        if password != confirm_password:
            print("passwords do not match!")
            exit(1)

        args.password = password

    main(args.username, args.email, args.password)
