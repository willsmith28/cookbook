import os

from starlette.config import Config

config = Config(".env")

SECRET_KEY = config.get("SECRET_KEY", cast=str, default=os.urandom(24).hex())

USER = config.get("POSTGRES_USER", cast=str, default="user")
PASSWORD = config.get("POSTGRES_PASSWORD", cast=str, default="password")
HOST = config.get("SQL_HOST", cast=str, default="localhost")
PORT = config.get("SQL_PORT", cast=int, default="5432")
DB = config.get("POSTGRES_DB", cast=str, default="")

CELERY_BROKER_URL = config.get(
    "CELERY_BROKER_URL", cast=str, default="redis://redis:6379/0"
)
CELERY_RESULT_BACKEND = config.get(
    "CELERY_RESULT_BACKEND", cast=str, default="redis://redis:6379/0"
)


DATABASE_URL = f"postgresql+asyncpg://{USER}:{PASSWORD}@{HOST}:{PORT}/{DB}"


ORGINS = (
    "http://localhost:8000",
    "http://localhost:8080",
)
